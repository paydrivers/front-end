const per_time_full = [{ "text": "00:00", "value": "00:00" }, { "text": "01:00", "value": "01:00" }, { "text": "02:00", "value": "02:00" }, { "text": "03:00", "value": "03:00" }, { "text": "04:00", "value": "04:00" }, { "text": "05:00", "value": "05:00" }, { "text": "06:00", "value": "06:00" }, { "text": "07:00", "value": "07:00" }, { "text": "08:00", "value": "08:00" }, { "text": "09:00", "value": "09:00" }, { "text": "10:00", "value": "10:00" }, { "text": "11:00", "value": "11:00" }, { "text": "12:00", "value": "12:00" }, { "text": "13:00", "value": "13:00" }, { "text": "14:00", "value": "14:00" }, { "text": "15:00", "value": "15:00" }, { "text": "16:00", "value": "16:00" }, { "text": "17:00", "value": "17:00" }, { "text": "18:00", "value": "18:00" }, { "text": "19:00", "value": "19:00" }, { "text": "20:00", "value": "20:00" }, { "text": "21:00", "value": "21:00" }, { "text": "22:00", "value": "22:00" }, { "text": "23:00", "value": "23:00" }];
const per_timer = [{ "text": "10", "value": "00:00" }, { "text": "01:00", "value": "01:00" }, { "text": "02:00", "value": "02:00" }, { "text": "03:00", "value": "03:00" }, { "text": "04:00", "value": "04:00" }, { "text": "05:00", "value": "05:00" }, { "text": "06:00", "value": "06:00" }, { "text": "07:00", "value": "07:00" }, { "text": "08:00", "value": "08:00" }, { "text": "09:00", "value": "09:00" }, { "text": "10:00", "value": "10:00" }, { "text": "11:00", "value": "11:00" }, { "text": "12:00", "value": "12:00" }, { "text": "13:00", "value": "13:00" }, { "text": "14:00", "value": "14:00" }, { "text": "15:00", "value": "15:00" }, { "text": "16:00", "value": "16:00" }, { "text": "17:00", "value": "17:00" }, { "text": "18:00", "value": "18:00" }, { "text": "19:00", "value": "19:00" }, { "text": "20:00", "value": "20:00" }, { "text": "21:00", "value": "21:00" }, { "text": "22:00", "value": "22:00" }, { "text": "23:00", "value": "23:00" }];

(function (params) {

	Vue.component('vue-admin-race', {
		template: '#template-admin-race',
		props: ['route'],
		data: function () {
			return {
				MapRaceCenter: [55.76, 37.64],
				MapRace: null,

				// шаг гонки 0, 1, 2, 3, 4, 5
				race_step: 0,

				// шаг установки точки финиша
				point_finish_step: 0,


				// нипуты и др хуета
				race_start_time: "00:00",
				race_start_time_options: per_time_full
			}
		},

		methods: {

			/**
			 * Устанвока точки финишп по карте
			 */
			setFinish: function () {
				switch (this.point_finish_step) {
					case 0:
						this.point_finish_step = 1;
						break;
					case 1:
						this.point_finish_step = 2;
						break;
					case 2:
						this.point_finish_step = 1;
						break;
				}
			},

			/**
			 * Созданние гонки
			 */
			createRace: function () {
				// тут будет запрос на создание гонки
				// TODO сохраняем, и меняем шаг
				this.race_step++;
			},

			goRaceScreen: function () {
				this.race_step = 4
			},

			goDahboardScreen: function () {
				app.$router.changeRoute({
					href: '/admin/dashboard',
					title: 'PayDrivers'
				});
			},

			/**
			 * Дестрой карты
			 */
			destroyMap: function () {
				let
					events = this.MapRaceEvents,
					map = this.MapRace;

				// дропаю события по карте
				events && events.removeAll();
				this.MapRaceEvents = null;

				// саму карту
				map && map.destroy();
				this.MapRace = null;
			},

			/**
			 * Создание карты
			 * @param {*} controls - контролсы
			 */
			createMap: function (controls) {
				ymaps.ready(function () {
					this['MapRace'] = new ymaps.Map("race-map", {
						center: this['MapRaceCenter'],
						controls: controls || [],
						zoom: 10
					});
					this.MapRaceEvents = this.MapRace.events.group();
				}.bind(this));
			},

		},
		watch: {
			/**
			 * Слежка за измененем кнопки отрисовки метки
			 * @param {*} curr - новое значение
			 * @param {*} old - старое значение
			 */
			'point_finish_step': function () {
				// убираем тыкание, потому что не варик
				this.MapRaceEvents
					&& this.MapRaceEvents
						.removeAll();

				// убираем тыкание, потому что не варик
				this.point_finish_step == 1
					&& this.MapRaceEvents
					&& this.MapRaceEvents
						.add('click', (e) => {
							// Получение координат щелчка
							setPoint.call(this, e.get('coords'));
							this.setFinish();
						});
			},

			/**
			 * Слежка за измененем шага
			 * @param {*} curr - новое значение
			 * @param {*} old - старое значение
			 */
			'race_step': function (curr, old) {

				// Если шаги, которые имеют карту
				if (curr >= 1 && curr < 5) {
					let
						is_prev_step = curr < old,
						isNewMap = curr == 4,
						isDropMap = is_prev_step || isNewMap;

					// если у нас шаг, который требует новую карту - то 
					isDropMap
						&& this.destroyMap();

					// если карты нет, то создаю
					!this.MapRace
						&& this.$nextTick(this.createMap.bind(this));

				} else {
					// если это первый шаг - то дропаю
					this.destroyMap();
				}
			}
		}
	});
	/**
	 * Установка точки оклейки
	 * @param {*} coords - кординаты
	 */
	function setPoint(coords) {
		this
			.MapRace
			.geoObjects
			.removeAll();
		let placemark = new ymaps.Placemark(
			coords,
			{ iconCaption: 'Финиш' },
			{
				iconImageHref: '../assets/imgs/finish-point.png',
				iconLayout: 'default#image',
			});
		this
			.MapRace
			.geoObjects
			.add(placemark);
	}
})();