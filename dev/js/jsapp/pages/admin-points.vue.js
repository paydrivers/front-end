
(function () {
	/**
	 * Точка оклейки
	 */
	Vue.component('vue-admin-points', {
		template: '#template-admin-points',
		props: ['route'],
		data: function () {
			return {
				cities: [{ id: '-1', text: 'Не выбрано' }],
				city_id: '-1',
				Map: null,
				// окно для показа создания онка
				isOpenPointWindow: false,
				pointsList: [],
				selectedPoint: null
			}
		},
		watch: {
			// слушаю изменение города
			'city_id': function () {
				app
					.HTTP({
						method: 'get',
						url: 'a/pasting_points/list' + (this.city_id > 0 ? ('/' + this.city_id) : ''),
						headers: {
							'Authorization': app.getToken()
						}
					})
					.then(function (data) {
						// меняю поинты
						this.pointsList = data.data.data;
						// сетчу на карту новые
						this.setPointsonMap();
					}.bind(this))
					.catch(function () {
					}.bind(this));
			}
		},
		created() {
			app
				.HTTP({
					method: 'get',
					url: 'a/pasting_points/list',
					headers: {
						'Authorization': app.getToken()
					}
				})
				.then(function (data) {
					this.pointsList = data.data.data;
				}.bind(this))
				.catch(function () {
				}.bind(this));

			// закрываю попап
			app.$event.$on('on-close-point-popup', function (isShow) {
				this.isOpenPointWindow = false;
				// app.$event.$emit(app.$eventsName.verificationOpen, (isShow) || null);
			}.bind(this));

			app.$event.$on('on-points-remove', function (id) {
				var isSetted = false;
				this.pointsList.forEach(function (point, index) {
					if (!isSetted && point.id == id) {
						this.pointsList.splice(index, 1);
						isSetted = true;
					}
				}.bind(this));
				this.setPointsonMap();
			}.bind(this));

			// закрываю попап
			app.$event.$on('on-points-save', function (data) {
				var isSetted = false;
				// ищу точки, которые совпадают с датой
				this.pointsList.forEach(function (point, index) {
					if (!isSetted && point.id == data.id) {
						this.pointsList[index] = data
						isSetted = true;
					}
				}.bind(this));

				//если не нашли ничего, то это новый элемент
				!isSetted && this.pointsList.push(data);
				this.setPointsonMap();
			}.bind(this));



		},
		mounted() {
			this.setBigMap();
		},
		methods: {
			// показываеть или скрывает. Шоет сбытие на верх, для сокрытия из вне
			openPointWindow: function (data) {
				this.selectedPoint = data;
				this.isOpenPointWindow = true;
			},
			setPointsonMap: function () {
				var features = [];
				this.objectManager && this.objectManager.removeAll();
				this.pointsList.forEach(function (point) {
					features.push(getFeature(point))
				});


				this.objectManager && this.objectManager.add({
					"type": "FeatureCollection",
					"features": features
				});

				setTimeout(() => {
					// равняню
					let bounds = this.objectManager && this.objectManager.getBounds();
					bounds && this.Map.setBounds(bounds);
				}, 0);
			},
			setBigMap: function () {
				$(function () {
					ymaps && ymaps.ready(function () {
						// свтвим центр, если новая - то мск, если редактирвоани - то уже есть координаты
						var
							center = [55.76, 37.64];


						//устанавливаени карту
						this.Map = new ymaps.Map("points-map", {
							center: center,
							controls: ['zoomControl', 'geolocationControl'],
							zoom: 18
						}, {
								minZoom: 3,
								maxZoom: 18
							});

						this.objectManager = new ymaps.ObjectManager({
							// Чтобы метки начали кластеризоваться, выставляем опцию.
							clusterize: true,
							// ObjectManager принимает те же опции, что и кластеризатор.
							gridSize: 32,
							clusterDisableClickZoom: true
						});


						this.Map.geoObjects.add(this.objectManager);
						this.setPointsonMap()


					}.bind(this));
				}.bind(this));

			}
		}
	});

	/**
	 * Попап для добавления точки оклейки
	 */
	Vue.component('vue-admin-points-popup', {
		template: '#template-admin-points-popup',
		props: ['route', 'selected', 'city_id'],
		watch: {
		},
		data: function () {
			let selected = this.selected;
			return {
				isShow: false,
				Map: null,
				// коорданаты, проверяем и дальне на существование [0,0]
				pointCoords: [selected ? selected.latitude : 0, selected ? selected.longitude : 0],
				// если это редактирвоание 0 то берем тот который редачим, иначе берем тот что выбран в селекте
				pointCity: selected ? selected.city_id : this.city_id,
				pointName: selected ? selected.name : '',
				pointAddress: selected ? selected.address : '',
				pointText: selected ? selected.description : ''
			}
		},
		methods: {
			// показываеть или скрывает. Шоет сбытие на верх, для сокрытия из вне
			showToggle: function (isShow) {
				app.$event.$emit('on-close-point-popup', isShow);
			},
			remove: function () {
				// сохраняем
				var id = this.selected.id;
				app
					.HTTP({
						method: 'delete',
						url: '/a/pasting_points/' + id,
						data: {
						},
						headers: {
							'Authorization': app.getToken()
						}
					}).then(function (data) {
						this.showToggle(false);
						app.$event.$emit('on-points-remove', id);
					}.bind(this))
			},
			save: function () {
				if (this.pointCoords.length < 2) {
					alert('Поставьте точку!')
					return false;
				}
				if (!this.pointName || !this.pointAddress || !this.pointText) {
					alert('Поля должны быть заполнены')
					return false;
				}
				// сохраняем
				app
					.HTTP({
						method: 'post',
						url: this.selected ? ('a/pasting_points/' + this.selected.id) : 'a/pasting_points/create',
						data: {
							"name": this.pointName,
							"address": this.pointAddress,
							"description": this.pointText,
							"city_id": this.pointCity,
							"latitude": this.pointCoords[0].toString(),
							"longitude": this.pointCoords[1].toString()
						},
						headers: {
							'Authorization': app.getToken()
						}
					})
					.then(function (data) {
						this.showToggle(false);
						app.$event.$emit('on-points-save', data.data.data);
					}.bind(this))
					.catch(function () {
						alert('Ошибка сохранениея точки оклейки')
					}.bind(this));
			},

			/**
			 * Уставновка карты
			 */
			setMap: function () {
				ymaps.ready(function () {
					// свтвим центр, если новая - то мск, если редактирвоани - то уже есть координаты
					var
						center = [55.76, 37.64],
						isPoint = this.pointCoords[0] && this.pointCoords[1];

					if (isPoint) {
						center = this.pointCoords;
					}

					//устанавливаени карту
					this.Map = new ymaps.Map("points-popup-map-set", {
						center: center,
						controls: ['zoomControl', 'geolocationControl'],
						zoom: 10
					});

					// ели естб кординаты - ставим точку оклейки
					if (isPoint) {
						setPoint.call(this, this.pointCoords);
					}
					// убираем тыкание, потому что не варик
					// this.Map
					// 	.events
					// 	.add('click', function (e) {
					// 		// Получение координат щелчка
					// 		setPoint.call(this, e.get('coords'))
					// 	}.bind(this));
				}.bind(this));
			}
		},
		created() {
			// устанавлива карту, по готовности
			this.setMap();
		},
	});

	/**
	 * Установка точки оклейки
	 * @param {*} coords - кординаты
	 */
	function setPoint(coords) {
		this.pointCoords = coords;
		this.Map
			.geoObjects
			.removeAll();
		this.Map
			.geoObjects
			.add(new ymaps.Placemark(coords, { iconCaption: 'Точка оклейки' }, {
				preset: 'islands#blueCircleDotIconWithCaption',
				iconCaptionMaxWidth: '100'
			}));
	}
	// получение феатури
	function getFeature(point) {
		return {
			"type": "Feature",
			"id": point.id,
			"geometry": {
				"type": "Point",
				"coordinates": [point.latitude, point.longitude]
			},
			"properties": {
				"balloonContentHeader": point.name,
				"balloonContentFooter": point.address
			}
		}
	}
})();