
(function (params) {
	// стрница с черым списком,
	Vue.component('vue-admin-blacklist', {
		template: '#template-admin-blacklist',
		props: ['table_row_items', 'route'],
		data() {
			return {

				// загрузка
				loading: true,

				table_title: 'Заблокированные участники РК',
				drivers_type_active: 1,
				//количетво водилеей
				drivers_count: 0
			}
		},
		created() {

			// Обновляем счетчик визуальный на элементе поика
			app.$event.$on('on-set-count-table', function (count) {
				this.drivers_count = count;
				this.loading = false;
			}.bind(this));

			app.$event.$on(app.$eventsName.onLoad, (flag) => this.loading = !flag);


		},
	});

})();