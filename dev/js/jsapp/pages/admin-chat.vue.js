
// Сам чат
(function () {

	const $LIST = '.chat-driver-messages-list';
	const $BODY = '.chat-body';

	Vue.component('vue-admin-chat', {
		template: '#template-admin-chat',
		props: [
			'route',
			'driver_messages_list',
			// непрочитанные
			'unread_messages_list',
			'unread_messages_total'
		],
		mixins: [vueMixins.getDriverList],
		data: function () {
			return {
				'current_messages_list': [],

				// загрузка
				loading: true,
				// текст в окне ввода
				textToDriver: '',
				// после добавления сообщения - скролл вниз
				afterSendScrollBottom: false,
				// если 0 то не выбрано ничего
				// хранит в себе элемент выбранного водителя
				driver_selected: null,
				// список водителей.
				//сюда должны будут приходить водители, для правого списка
				// и колическо всего пришло
				driverList: this.driver_messages_list,
				// список непрочитанных сообщений
				messagesUnRreadCount: this.unread_messages_total,
				messagesListUnRead: this.unread_messages_list,
				// количество записей
				countRecords: 0,
				// признак следующей страницы
				isMoreMessages: null,
				// зацпущена ли верификацияы
				isVerificationRun: true
			}
		},

		computed: {
			classDisabledVerification: function name(params) {
				return (!this.driver_selected || this.isVerificationRun) ? 'disabled' : '';
			},
			// дисейбл кнопок 
			classDisabled: function () {
				return !this.driver_selected ? 'disabled' : '';
			},
		},

		created() {

			// при создании - сраду дергаю водителей
			this.getDriverList.call(this, 'chat/channels/drivers');


			this._driverChatController = _driverChatController.bind(this)
			this._onMessageNew = _onMessageNew.bind(this)
			this._onLoadDriveList = _onLoadDriveList.bind(this)
			this._resendMessage = _resendMessage.bind(this)
			this.messageListReplace = messageListReplace.bind(this)
			this._messageAdd = _messageAdd.bind(this)
			this._verificationStatusUpdate = _verificationStatusUpdate.bind(this)
			this._verificationUpdate = _verificationUpdate.bind(this)
			this._updateMessage = _updateMessage.bind(this)
			this.currentDriverStatusVerification = currentDriverStatusVerification.bind(this);

			// событие завершенеи загрузки водил + для сброса эске
			app.$event
				// esc, кнопка мыши на пустом поле
				.$on(app.$eventsName.onGlobalClose, this._driverChatController)
				// приходит сообшение
				.$on(app.$eventsName.onAddChatMessage, this._onMessageNew)
				// загруили таблицу
				.$on(app.$eventsName.onLoad, this._onLoadDriveList)
				// resend message
				.$on(app.$eventsName.resend, this._resendMessage)

				// подгружаю (заменяю) список водителей, или просто он пустой
				.$on('on-messages-list-load', this.messageListReplace)
				// нвоое сообщение
				.$on('on-messages-list-add-new', this._messageAdd)

				// обновление соощения верификации
				.$on('on-verification-status-change', this._verificationStatusUpdate)
				.$on('on-message-update-verification', this._verificationUpdate)
				// добавление айтемов верификации
				.$on('on-verification-add', _updateMessage)
				.$on('on-message-verification-status-change', this.currentDriverStatusVerification)
		},

		mounted() {
			this.messageBody = $($BODY);
		},


		// при апдейте данных - гляжу снова, если мы 
		// подходим к концу страницы, а следующие есть
		updated: function () {

			// отправка сообщения или аттача
			if (this.afterSendScrollBottom === true) {
				$($LIST)
					.imagesLoaded((imgLoad) => {
						setTimeout(() => {
							this.messageBody.scrollTop($('.chat-driver-messages-list').height());
						}, 0);
						this.afterSendScrollBottom = false;
					});
			}
		},

		destroyed() {
			// событие завершенеи загрузки водил + для сброса эске
			// событие завершенеи загрузки водил + для сброса эске
			app.$event
				// esc, кнопка мыши на пустом поле
				.$off(app.$eventsName.onGlobalClose, this._driverChatController)
				// приходит сообшение
				.$off(app.$eventsName.onAddChatMessage, this._onMessageNew)
				// загруили таблицу
				.$off(app.$eventsName.onLoad, this._onLoadDriveList)
				// resend message
				.$off(app.$eventsName.resend, this._resendMessage)

				// подгружаю (заменяю) список водителей, или просто он пустой
				.$off('on-messages-list-load', this.messageListReplace)
				// нвоое сообщение
				.$off('on-messages-list-add-new', this._messageAdd)

				// обновление соощения верификации
				.$off('on-verification-status-change', this._verificationStatusUpdate)
				.$off('on-message-update-verification', this._verificationUpdate)
				// добавление айтемов верификации
				.$off('on-verification-add', _updateMessage)
				.$off('on-message-verification-status-change', this.currentDriverStatusVerification)
		},
		methods: {
			// "Скачать все фото"
			downloadAllDriverPhotos: _downloadAllDriverPhotos,
			// Отправка сообщения с аттачем
			sendMessageAttachment: _sendMessageAttachment,
			// открываю чат, по лику на водилу
			driverChatController: _driverChatController,
			// Запрос фотографии
			sendMessageRequest: _sendMessageRequest,
			// Для инфините-скролла для часа
			onScrollChatList: _onScrollChatList,
			// Отправка текстового сообщения
			sendMessageText: _sendMessageText,
			// Забанить драйвера
			bunDriver: _bunnedXHRDriver
		}
	});



	/**
	 * Обноыление сообщений, после верифкикации
	 * @param {*} data 
	 */
	function _updateMessage(data) {
		// если это верификайшен стоп или старт
		if (data.data.type == 'verification_expires') {
			console.log('Изменение статуса верификации', data.data.data);
			app.$event.$emit('on-message-update-verification', data.data.data);
		}

	}

	function currentDriverStatusVerification(data) {
		this.isVerificationRun = data;
	}


	function _verificationStatusUpdate(currentVerification) {
		let
			// ищу из всех сообщений - нужное мне
			$msgsAttachment = this.current_messages_list
				.filter(function (itm, i) {
					return itm.parameters && itm.parameters.verification && itm.parameters.verification.photo && itm.parameters.verification.photo.status
					itm.parameters.verification.id == currentVerification.id
				}),
			$msgsVerification = this.current_messages_list
				.filter(function (itm, i) {
					return itm.data.verification && itm.data.verification &&
						itm.data.verification.id == currentVerification.id
				});

		if ($msgsAttachment && $msgsAttachment.length) {
			$msgsAttachment.forEach(function (mess) {
				if (mess.parameters && mess.parameters.verification && mess.parameters.verification.photo.status) {
					mess.parameters.verification.photo.status = currentVerification.status;
				}
			}.bind(this));
			this.$forceUpdate();
		}


		if ($msgsVerification && $msgsVerification.length) {
			$msgsVerification.forEach(function (mess) {
				if (mess.data && mess.data.verification) {
					// var _mess = Object.assign({}, mess);
					mess.data.verification.status = currentVerification.status;
				}
			}.bind(this));
			this.$forceUpdate();

		}
	}
	/**
	 * Обновление всех верификаций
	 * @param {*} verData 
	 */
	function _verificationUpdate(verData) {
		let
			// ищу из всех сообщений - нужное мне
			$msgs = this.current_messages_list
				.filter(function (itm, i) {
					return itm.data.verification && itm.data.verification &&
						itm.data.verification.id == verData.verification_id
				});
		if (!$msgs || !$msgs.length) {
			return false;
		}
		$msgs.forEach(function (mess) {
			if (mess.data && mess.data.verification) {
				// var _mess = Object.assign({}, mess);
				mess.data.verification.expires_in = verData.expires_in;
				mess.data.verification.status = verData.status;
			}
		}.bind(this));
		this.$forceUpdate();
	}

	/**
	 * Добавляю новое сообщение
	 * @param {*} messageAdded 
	 */
	function _messageAdd(messageAdded) {
		this.current_messages_list.unshift(messageAdded);
	}

	/**
	 * Инифнитие сктролл
	 */
	function _onScrollChatList(event) {
		// еслиэто не инифините скролл - то могу обнулить
		// если есть что грузить, то грузим
		if (isNeedScrollHeight() && this.isMoreMessages) {
			getMessagesBySelectedDriver.call(this, {
				timestemp: this.isMoreMessages,
			});
			// загрузили новую
			this.isMoreMessages = null;
		}
	}


	/**
	 * Замена списка сообщений
	 * @param {*} current_messages_list 
	 */
	function messageListReplace(current_messages_list) {
		this.current_messages_list = current_messages_list;
		console.log('-- Messages all update')
	}

	/**
	 * Качаю все фотографии
	 */
	function _downloadAllDriverPhotos() {
		let
			xhr = app.HTTP,
			xhrHeaders = { 'Authorization': app.getToken() },
			xhrOptions = {
				method: 'post',
				url: 'users/profile/download-attachments',
				headers: xhrHeaders,
				data: {
					users_ids: [this.driver_selected.user.id]
				}
			};

		// загружаю фотоку
		xhr(xhrOptions)
			.then(function (data) {
				data.data.data.forEach(function (item) {
					window.open(item.path, "_blank");
				});
			})

	}


	/**
	 * При загрузки затсрны
	 * @param {*} flag 
	 */
	function _onLoadDriveList(flag) {
		this.loading = !flag;
		let
			driverItemId = localStorage.getItem('chat_selected_item'),
			driverItemIndex = false;

		// если есть выбранный водитель - иду по списку и ищу 
		// нужный индекс, сравнивая по айдишником сообщения
		driverItemId && !driverItemIndex && this.driverList
			.forEach((d, i) => {
				// ищу индекс с той же мессагой
				if (!driverItemIndex && d.user.id == driverItemId) {
					_driverChatController.call(this, d);
				}
			});
	}


	/**
	 * Переотправка ообщения
	 * @param {*} message 
	 */
	function _resendMessage(message) {
		switch (message.driver_message.type) {
			case 'verification':
				_sendMessageRequest.call(this, message.driver_message);
				break;
			case 'attachment':
				_sendMessageAttachment.call(this, message.driver_message);
				break;
			case 'message':
				_sendMessageText.call(this, message.driver_message);
				break;
		}
	}

	function _sendMessageRequest(resendMessage) {
		this.afterSendScrollBottom = true;

		let
			// клон сообщенийsendMessage
			cloneAllMessagesList = [].concat([], this.current_messages_list),
			// уид для сообщени
			uuid = resendMessage && resendMessage.uuid ? resendMessage.uuid : app.getHash(),
			// идентификатор водилы
			driverId = resendMessage && resendMessage.user_id ? resendMessage.user_id : this.driver_selected.user.id,
			// данны для отправки
			sendData = {
				text: resendMessage && resendMessage.data && resendMessage.data.text ? resendMessage.data.text : (this.textToDriver || app.defaultVerificationText),
				verification: {
					expires_in: '',
					status: 'pending'
				}
			},
			// смещение для утс
			tzOffest = new Date().getTimezoneOffset() * 60 * 1000,
			// сообщение в целом
			messageAdded = resendMessage && resendMessage.uuid ? resendMessage : {
				created_at: new Date((new Date().getTime() - tzOffest) / 1000).getTime(),
				sent_by_manager: true,
				status: "sending",
				data: sendData,
				type: "verification",
				user_id: driverId,
				uuid: uuid
			},
			// данныке для отправки
			messageData = {
				"uuid": uuid,
				"message": sendData.text || app.defaultVerificationText
			},
			// хедер для авторизщации
			headersAuth = { 'Authorization': app.getToken() },
			// хшр
			xhr = app.HTTP,
			// опции для запроса
			xhrOptions = {
				method: 'post',
				url: ['chat/messages/verification/send/', this.driver_selected.user.id].join(''),
				headers: headersAuth,
				data: messageData
			};

		// чищу поле ввода
		this.textToDriver = '';

		// // доабляю в список новое сообщенеи, со статусом доствки
		_onMessageNew.call(this, messageAdded);

		app.$event.$emit('on-message-verification-status-change', true);

		// отсылаю сообщение
		xhr(xhrOptions)
			.then((data) => {
				// устанавливаю статус доставки
				setStatusDelivered.call(this, data.data.data);
				attachLoader.call(this);
			})
			.catch(function name(params) {
				attachLoader.call(this);
				setStatusDelivered.call(this, messageAdded, true);

			}.bind(this));

		// очищаю много данныъ
		cloneAllMessagesList = [];

	}


	/**
	 * Отправка текстового сообщения
	 * Собирает сообщение для фронта и для отправки.
	 * Выводит и отправляет, и далее - меняется статус доставки
	 * Обновляю список водителей
	 */
	function _sendMessageText(resendMessage) {

		this.afterSendScrollBottom = true;

		let
			// клон сообщенийsendMessage
			cloneAllMessagesList = [].concat([], this.current_messages_list),
			// уид для сообщени
			uuid = resendMessage && resendMessage.uuid ? resendMessage.uuid : app.getHash(),
			// идентификатор водилы
			driverId = resendMessage && resendMessage.user_id ? resendMessage.user_id : this.driver_selected.user.id,
			// данны для отправки
			sendData = {
				text: resendMessage && resendMessage.data && resendMessage.data.text ? resendMessage.data.text : this.textToDriver
			},
			// смещение для утс
			tzOffest = new Date().getTimezoneOffset() * 60 * 1000,
			// сообщение в целом
			messageAdded = (resendMessage && resendMessage.uuid) ? resendMessage : {
				created_at: new Date((new Date().getTime() - tzOffest) / 1000),
				sent_by_manager: true,
				status: "sending",
				data: sendData,
				type: "message",
				user_id: driverId,
				uuid: uuid
			},
			// данныке для отправки
			messageData = {
				"uuid": uuid,
				"message": sendData.text
			},
			// хедер для авторизщации
			headersAuth = { 'Authorization': app.getToken() },
			// хшр
			xhr = app.HTTP,
			// опции для запроса
			xhrOptions = {
				method: 'post',
				url: ['chat/messages/simple/send/', driverId].join(''),
				headers: headersAuth,
				data: messageData
			};

		// чищу поле ввода
		this.textToDriver = '';

		// доабляю в список новое сообщенеи, со статусом доствки
		_onMessageNew.call(this, messageAdded);

		// отсылаю сообщение
		xhr(xhrOptions)
			.then((data) => {
				// устанавливаю статус доставки
				setStatusDelivered.call(this, data.data.data);
				attachLoader.call(this);
			})
			.catch(function () {
				setStatusDelivered.call(this, messageAdded, true);
				attachLoader.call(this)
			}.bind(this));
		// очищаю много данныъ
		cloneAllMessagesList = [];
	}

	/**
	 * Отправка сообщие с аттачем
	 * Собирает файл
	 */


	function _sendMessageAttachment(resendMessage) {
		this.loading = true;
		this.afterSendScrollBottom = true;
		let
			// jq инут- файл
			$btnUpload = $('#chat-file'),
			// инпут-файл
			$FILE = $btnUpload[0].files[0],
			// выделенный водила
			driverId = resendMessage && resendMessage.user_id ? resendMessage.user_id : this.driver_selected.user.id,
			attachment = new FormData(),
			reader = new FileReader(),
			uuid = resendMessage && resendMessage.uuid ? resendMessage.uuid : app.getHash(),
			// если нет под такого юзера - то создаю
			history_item = [],
			// смещение для утс
			xhr = app.HTTP,
			xhrHeaders = { 'Authorization': app.getToken() },
			xhrOptions = {
				method: 'post',
				url: 'chat/messages/attachment/upload/' + driverId,
				headers: xhrHeaders
			};

		// доабвляю в форм дату - файл
		attachment.append('attachment', $FILE);
		// записываю в данные - наш файл
		xhrOptions.data = attachment;

		// читаю файл
		reader.readAsDataURL($FILE);

		// загружаю фотку, отправляя
		reader.onloadend = fileSending.bind(this, reader, driverId, uuid);

		// загружаю фотоку
		xhr(xhrOptions)
			.then(function (data) {
				let xhrDataPath = data.data && data.data.data && data.data.data.path;
				xhrOptions.url = 'chat/messages/attachment/send/' + driverId
				xhrOptions.data = {
					"uuid": uuid,
					"attachment": xhrDataPath
				}
				xhrDataPath && xhr(xhrOptions)
					.then(function (attachment) {
						// меняю статус доставки
						setStatusDelivered.call(this, attachment.data.data);
						attachLoader.call(this);
					}.bind(this))
					.catch(function () {
						attachLoader.call(this)
						setStatusDelivered.call(this, this._msg, true);
					}.bind(this));
			}.bind(this))
			.catch(attachLoader.bind(this));
	}

	/**
	 * Отправка файла
	 * @param {*} reader 
	 * @param {*} driverId 
	 * @param {*} uuid 
	 */
	function fileSending(reader, driverId, uuid) {
		this.loading = true;
		let
			// смещение для утс
			tzOffest = new Date().getTimezoneOffset() * 60 * 1000,
			msg = {
				created_at: new Date((new Date().getTime() - tzOffest) / 1000),
				sent_by_manager: true,
				status: "sending",
				data: {
					attachment: reader.result
				},
				type: "attachment",
				user_id: driverId,
				uuid: uuid
			}
		this._msg = msg;
		_onMessageNew.call(this, msg);
	};

	/**
	 * Контрлер откртого чата водителя
	 * @param {*} driver_item 
	 */
	function _driverChatController(driver_item) {

		// выходим если зарещабт всплывать
		if (app.isBubblingDrop()) {
			return false;
		}
		// не выделю тот же
		if (this.driver_selected && driver_item && (this.driver_selected.user.id === driver_item.user.id)) {
			// если то деж водитель - то нничего не делаем
			return false;
		}

		// this.current_messages_list = [];
		// app.$event.$emit('on-messages-list-load', []);

		this.loading = true;
		this.afterSendScrollBottom = false;
		this.isVerificationRun = true;


		if (!driver_item) {
			getterDriverItems.call(this);
			return false;
		}

		// 
		let
			xhr = app.HTTP,
			xhrHeaders = { 'Authorization': app.getToken() },
			xhrOptions = {
				method: 'get',
				url: 'chat/channels/' + driver_item.user.id + '/info',
				headers: xhrHeaders,
			};


		// получаю данные о верификации
		xhr(xhrOptions)
			.then(getterDriverItems.bind(this))
			.catch(function () {
				this.driver_selected = null;
				alert('Ошибка загрузки чата')
				this.loading = false;
			}.bind(this));

		/**
		 *  метод для получения данных 
		 * 	о водителе или его очистчка
		 * @param {*} data 
		 */
		function getterDriverItems(data) {

			if (data && data.data && data.data.data && data.data.data.verification) {
				this.isVerificationRun = data.data.data.verification.exists;
				console.log(this.isVerificationRun)
			}

			if (driver_item) {
				this.driver_selected = driver_item;
				cacheSelectedDriver(this);
				localStorage.setItem('chat_selected_item', driver_item.user.id);
			} else {
				// передаю старый, пока не очистили
				cacheSelectedDriver(this, this.driver_selected);
				// и яищаю
				this.driver_selected = null;
				localStorage.setItem('chat_selected_item', '');
			}


			//очищаю собщеня
			// this.current_messages_list = [];
			app.$event.$emit('on-messages-list-load', [])
			app.timeRequest && clearInterval(app.timeRequest)

			if (this.driver_selected) {
				getMessagesBySelectedDriver.call(this, {
					timestemp: null
				});
			} else {
				this.loading = false;
			}
		}

	}


	/**
	 * Установить прочитанными сообщения
	 */
	function setMessagesRead() {

		if (!this.driver_selected || !this.driver_selected.user) {
			alert('Выберите водителя!')
			return false;
		}

		let
			// все не прочтенные сообщения
			// Айдишник открытого чата с водителем
			driverId = this.driver_selected && this.driver_selected.user && this.driver_selected.user.id,
			// адрес урла
			urlFull = 'chat/messages/unread/mark_as_read/' + driverId,
			// количество непрочитанных сообщений даного юзера
			unReadedMessages = this.messagesListUnRead[driverId],
			// хедер для авторизщации
			headersAuth = { 'Authorization': app.getToken() },
			// хшр
			xhr = app.HTTP,
			// опции для запроса
			xhrOptions = {
				method: 'post',
				url: urlFull,
				headers: headersAuth
			};

		driverId && xhr(xhrOptions)
			.then(function () {

				//  ставлю прочитанными статусы
				let
					// ищу не прочитанные от водителя
					unreaded = this.current_messages_list.filter((item) => !item.sent_by_manager && item.status != 'read');

				// ставлю их прочеитанными
				unreaded.forEach((item) => item.status = 'read');

				// перезапрашиваю колечества айтемов, если были не прочитанные
				unreaded.length && app.$event.$emit(app.$eventsName.onMessageCounterUpdate);
			}.bind(this));
	}


	/**
	 * Получаю сообщения по выбранному юзеру, и переданными опциями.
	 * Запускает рекурсивно, при событии того что загрзка первая, 
	 * 						а  после нее - нужна еще одна загрузка
	 * 
	 * @param options.timestemp - таймстемп для некст загрузки, либо если null - то это первая
	 * @param options.isScroll - признак того, надо ли скроллить вниз
	 */
	function getMessagesBySelectedDriver(options) {
		this.loading = true;

		let
			// айдищник водителя
			driverId = this.driver_selected.user.id,
			// тамстемп переданный
			timestemp = options.timestemp,
			// первая загрузка - без тамстемпа
			isFirstLoad = !timestemp,
			// если етсь тамстемп - то это загрузка не первой переписки, если нет - то пустая строка
			urlPartTimestemp = timestemp && ['/', parseFloat(timestemp)].join('') || '',
			// добавляю айдишник водилы и, если есть - часть утра с таймстемпом
			urlFull = ['chat/messages/list/', driverId, urlPartTimestemp].join(''),
			// хедер для авторизщации
			headersAuth = { 'Authorization': app.getToken() },
			// хшр
			xhr = app.HTTP,
			// опции для запроса
			xhrOptions = {
				url: urlFull,
				method: 'get',
				headers: headersAuth
			};
		xhr(xhrOptions)
			.then((response) => {
				let
					// Инфа о загруженной странцы
					messagesData = response.data.data,
					// сами сообщения
					messagesList = messagesData.messages,
					// Признак наличия следующих страниц
					messagesListMore = messagesData.last_id,
					// Определяем, это это ервая загрузка, и после нее список имеет продолженени
					hasMoreMessages = messagesListMore && isNeedScrollHeight(),
					// клонирую сообщения, для их дорабоьки
					cloneAllMessagesList = [].concat([], this.current_messages_list);

				// время для следующей страницы
				this.isMoreMessages = messagesListMore;

				// Передаю в главн компонент данные для чата
				app.$event.$emit('on-messages-list-load', (cloneAllMessagesList.concat(messagesList)).sort(app.sortByCreatedAt))

				// читаю
				isFirstLoad && this.driver_selected && setMessagesRead.call(this);

				cloneAllMessagesList = [];

				// запомниаю высоты
				let
					$list = $($LIST),
					$body = $($BODY),
					// текущие высоты
					bodyOffsetHeight = $body.height(),
					listOffsetHeight = $list.height(),
					// кешируешь новую
					oldListOffsetHeight = listOffsetHeight,
					// есть шаг
					isTick = this.isMoreMessages && (bodyOffsetHeight >= listOffsetHeight),
					// это либо первая загрузка, либо когдавысота не подходит
					// те. инфините скролл
					isNexTick = isFirstLoad || isTick,
					// это бесконечный скролл
					isInfiliteScroll = isFirstLoad || (!isNexTick && isNeedScrollHeight());

				// Если это верх скролла, и шага уже нет или первая загрузка
				// после перерисовки - скрллим и орубаем лоад
				// и больше нихуя не делаем
				if (isInfiliteScroll) {
					this.$nextTick(function () {
						attachLoader.call(this, !isNexTick ? $list.height() - oldListOffsetHeight : false);
					});

					// если первая загрузка,
					// то по любому матаем вниз
					if (isFirstLoad) {
						this.$nextTick(function () {
							attachLoader.call(this, false, true);
						});
					} else {
						isNexTick = false;
					}
				}

				// по идее тут уде отрисовка, ели л
				isNexTick &&
					this.$nextTick(function () {
						listOffsetHeight = $list.height();
						chatScroll(this);

						// если больше нет чего гурзить
						!this.isMoreMessages && attachLoader.call(this);

						// если есть что грузить и грузить надо
						if (this.isMoreMessages && (bodyOffsetHeight >= listOffsetHeight)) {
							// то гружу еще
							getMessagesBySelectedDriver.call(this, {
								timestemp: this.isMoreMessages
							});
						} else {
							attachLoader.call(this);
						}
					});
			})
			.catch(function () {
				// загруака для ладера
				attachLoader.call(this);
			}.bind(this));

	}



	function chatScroll(scrll) {
		setTimeout(() => {
			$($BODY).scrollTop(scrll || $('.chat-driver-messages-list').height());
		}, 0);
	}

	/**
	 * Фотки загружены ли?
	 * прогоняес
	 */
	function attachLoader(scrll, isFirstLoad) {
		scrll && chatScroll(scrll)

		$('.chat-message__attachment:not(.is-loading)')
			.addClass('is-loading')
			.imagesLoaded()
			.done(() => {
				// загрузились - скрываю всю хуйню
				!scrll && chatScroll(false);
				if (!isFirstLoad) {
					this.loading = false;
				}
			});
	}


	// Одиночные сообщетньки message
	// @props driver_messages_list - список сообщенй для данного польщователя(водителя)
	// @props driver_selected - выбранный водитель,
	// @props driver_message - сообщение воодитлю
	// @props get_date_format_fn - надо получиьь геоданные
	function _bunnedXHRDriver(driver) {
		var isBlocked = driver.user.blocked;
		app.bunController({
			state: !isBlocked,
			users_ids: [driver.user.id],
			reason: isBlocked ? 'Забанен из чата' : 'без причины'
		})
			.then(function (resp) {
				//делаю подмену на тот который стоит уже
				this.driver_selected.user.blocked = !isBlocked;
				alert(!isBlocked ? 'Водитель забанены!' : 'Водитель разбанен!');
			}.bind(this))
			.catch((function () {
				this.driver_selected = null;
				alert('Ошибка бана.');
			}).bind(this));
	}



	/**
	 * Метод для добавления нового сообщения 
	 * 		в общий список сообщений
	 * Затем - сортирую водил
	 * @param {*} messageAdded 
	 */
	function _onMessageNew(messageAdded) {
		// добавляю сообщение и обновляю

		let
			fildedItem = this.current_messages_list.find(function (itm) {
				return itm.uuid == messageAdded.uuid;
			}),
			fn_goRead = $.debounce(_goRead, 1000).bind(this),
			isSelectedDriver = app.driver_selected.user.id === messageAdded.user_id,
			// если разрешаем добавление
			// если это верфиикаия НЕ с пендингом, то занчит -добавляем,
			// либо это обычное сообщение
			isAddToMessageArray = (messageAdded.data.verification && messageAdded.data.verification.status !== 'pending' && messageAdded.type === 'verification') || (messageAdded.type !== 'verification');

		// отрубаю добавление и сортировку уводил, если выпадает верификация==пендинг
		if (!isAddToMessageArray) {
			return false
		}
		// есл ито не выделенный таб, у пришедшенго?
		// если такого нет, то добавляю на 1 место
		if (!fildedItem) {
			if (isSelectedDriver) {
				// если статус не пендинг и это верификация
				// app.$event.$emit('on-messages-list-load', [messageAdded].concat(this.current_messages_list, []))
				app.$event.$emit('on-messages-list-add-new', messageAdded)
			}
		}

		// сортирую правый список, передавая первое(новое) сообщение)
		setSortDriverList.call(this, messageAdded);

		// кеширую выбранного водителя в глобальнй конетст
		cacheSelectedDriver(this);

		// если эо не менеджера собщения
		if (!messageAdded.sent_by_manager) {
			// читаю
			fn_goRead(messageAdded);
			// считаю
			app.$event.$emit(app.$eventsName.onMessageCounterUpdate);
		}
	}

	/**
	 * Начинаем чтнение непрочитанных
	 * @param {*} messageAdded 
	 */
	function _goRead(messageAdded) {
		!messageAdded.sent_by_manager && this.driver_selected && setMessagesRead.call(this);
	}



	/**
	 * Сортировка списка водителей слева, по времени
	 * @param {*} messageAdded - messageAdded
	 */
	function setSortDriverList(messageAdded) {
		let
			// список водил для чата
			driverList = [].concat([], this.driverList),
			// выделенный юзер
			driverItemIndex = -1,
			// драйвер айди
			driverItemId = messageAdded
				&& messageAdded.user_id;

		// если есть выбранный водитель - иду по списку и ищу 
		// нужный индекс, сравнивая по айдишником сообщения
		driverItemId && driverList
			.forEach((d, i) => {
				// ищу индекс с той же мессагой
				if (d.user.id == driverItemId) {
					driverItemIndex = i;
					return true;
				}
			});

		// помещаю первой
		if (driverItemIndex >= 0) {
			// подставляю новый текс для водителей
			var itemDriver = driverList.splice(driverItemIndex, 1);
			if (itemDriver && itemDriver[0]) {
				itemDriver[0].message = Object.assign({}, messageAdded);
				driverList = [].concat(itemDriver, driverList);
			}

		}
		// обновляю сортировку под дате, обновив сообщение
		this.driverList = driverList;

	}


	/**
	 * Установить статус доставки
	 * @param {*} message  - сообщение
	 */
	function setStatusDelivered(message, isErr) {
		let
			// ищу из всех сообщений - нужное мне
			index = null,
			msg = this.current_messages_list
				.filter(function (itm, i) {
					index = i;
					return itm.uuid == message.uuid;
				}),
			$message = msg && msg.length && msg[0];

		if (!message) {
			return false;
		}

		// если дата есть
		if ($message.data && $message.status && $message.created_at) {
			// обнволяю данные новые
			$message.data = message.data;
			let _v = message.data.verification;
			// если это вериикация и там нет статуса - то проаставляем(записал для женька)
			if (_v && !_v.status) {
				$message.data.verification.status = 'pending';
			}
			// если есть ошибка - то добавляем класс "фалй"
			$message.status = isErr ? 'fail' : message.status;
			// айди юзера
			$message.user_id = message.user_id;
			// колг=гда создано
			$message.created_at = message.created_at;
		}

	}

	/**
	 * Кеширую список водил, для того тчобы потом, 
	 * если закрыли диалог - обновть данные
	 * @param {*}  
	 */
	function cacheSelectedDriver($this, driver_selected) {
		var selected = driver_selected || $this.driver_selected;
		selected && (app.driver_selected = Object.assign({}, selected));
	}

	// нужна ли загрузка, судя по высоте 
	function isNeedScrollHeight() {
		return document.querySelector($BODY).scrollTop <= app.infinityScrollTargetHeight
	}


})();
