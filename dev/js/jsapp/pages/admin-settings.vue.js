const per_km = [{ "text": "0.0", "value": "0.0" }, { "text": "0.1", "value": "0.1" }, { "text": "0.2", "value": "0.2" }, { "text": "0.3", "value": "0.3" }, { "text": "0.4", "value": "0.4" }, { "text": "0.5", "value": "0.5" }, { "text": "0.6", "value": "0.6" }, { "text": "0.7", "value": "0.7" }, { "text": "0.8", "value": "0.8" }, { "text": "0.9", "value": "0.9" }, { "text": "1.0", "value": "1.0" }, { "text": "1.1", "value": "1.1" }, { "text": "1.2", "value": "1.2" }, { "text": "1.3", "value": "1.3" }, { "text": "1.4", "value": "1.4" }, { "text": "1.5", "value": "1.5" }, { "text": "1.6", "value": "1.6" }, { "text": "1.7", "value": "1.7" }, { "text": "1.8", "value": "1.8" }, { "text": "1.9", "value": "1.9" }, { "text": "2.0", "value": "2.0" }, { "text": "2.1", "value": "2.1" }, { "text": "2.2", "value": "2.2" }, { "text": "2.3", "value": "2.3" }, { "text": "2.4", "value": "2.4" }, { "text": "2.5", "value": "2.5" }, { "text": "2.6", "value": "2.6" }, { "text": "2.7", "value": "2.7" }, { "text": "2.8", "value": "2.8" }, { "text": "2.9", "value": "2.9" }, { "text": "3.0", "value": "3.0" }, { "text": "3.1", "value": "3.1" }, { "text": "3.2", "value": "3.2" }, { "text": "3.3", "value": "3.3" }, { "text": "3.4", "value": "3.4" }, { "text": "3.5", "value": "3.5" }, { "text": "3.6", "value": "3.6" }, { "text": "3.7", "value": "3.7" }, { "text": "3.8", "value": "3.8" }, { "text": "3.9", "value": "3.9" }, { "text": "4.0", "value": "4.0" }, { "text": "4.1", "value": "4.1" }, { "text": "4.2", "value": "4.2" }, { "text": "4.3", "value": "4.3" }, { "text": "4.4", "value": "4.4" }, { "text": "4.5", "value": "4.5" }, { "text": "4.6", "value": "4.6" }, { "text": "4.7", "value": "4.7" }, { "text": "4.8", "value": "4.8" }, { "text": "4.9", "value": "4.9" }, { "text": "5.0", "value": "5.0" }, { "text": "5.1", "value": "5.1" }, { "text": "5.2", "value": "5.2" }, { "text": "5.3", "value": "5.3" }, { "text": "5.4", "value": "5.4" }, { "text": "5.5", "value": "5.5" }, { "text": "5.6", "value": "5.6" }, { "text": "5.7", "value": "5.7" }, { "text": "5.8", "value": "5.8" }, { "text": "5.9", "value": "5.9" }, { "text": "6.0", "value": "6.0" }, { "text": "6.1", "value": "6.1" }, { "text": "6.2", "value": "6.2" }, { "text": "6.3", "value": "6.3" }, { "text": "6.4", "value": "6.4" }, { "text": "6.5", "value": "6.5" }, { "text": "6.6", "value": "6.6" }, { "text": "6.7", "value": "6.7" }, { "text": "6.8", "value": "6.8" }, { "text": "6.9", "value": "6.9" }, { "text": "7.0", "value": "7.0" }, { "text": "7.1", "value": "7.1" }, { "text": "7.2", "value": "7.2" }, { "text": "7.3", "value": "7.3" }, { "text": "7.4", "value": "7.4" }, { "text": "7.5", "value": "7.5" }, { "text": "7.6", "value": "7.6" }, { "text": "7.7", "value": "7.7" }, { "text": "7.8", "value": "7.8" }, { "text": "7.9", "value": "7.9" }, { "text": "8.0", "value": "8.0" }, { "text": "8.1", "value": "8.1" }, { "text": "8.2", "value": "8.2" }, { "text": "8.3", "value": "8.3" }, { "text": "8.4", "value": "8.4" }, { "text": "8.5", "value": "8.5" }, { "text": "8.6", "value": "8.6" }, { "text": "8.7", "value": "8.7" }, { "text": "8.8", "value": "8.8" }, { "text": "8.9", "value": "8.9" }, { "text": "9.0", "value": "9.0" }, { "text": "9.1", "value": "9.1" }, { "text": "9.2", "value": "9.2" }, { "text": "9.3", "value": "9.3" }, { "text": "9.4", "value": "9.4" }, { "text": "9.5", "value": "9.5" }, { "text": "9.6", "value": "9.6" }, { "text": "9.7", "value": "9.7" }, { "text": "9.8", "value": "9.8" }, { "text": "9.9", "value": "9.9" }, { "text": "10.0", "value": "10.0" }, { "text": "10.1", "value": "10.1" }, { "text": "10.2", "value": "10.2" }, { "text": "10.3", "value": "10.3" }, { "text": "10.4", "value": "10.4" }, { "text": "10.5", "value": "10.5" }, { "text": "10.6", "value": "10.6" }, { "text": "10.7", "value": "10.7" }, { "text": "10.8", "value": "10.8" }, { "text": "10.9", "value": "10.9" }, { "text": "11.0", "value": "11.0" }, { "text": "11.1", "value": "11.1" }, { "text": "11.2", "value": "11.2" }, { "text": "11.3", "value": "11.3" }, { "text": "11.4", "value": "11.4" }, { "text": "11.5", "value": "11.5" }, { "text": "11.6", "value": "11.6" }, { "text": "11.7", "value": "11.7" }, { "text": "11.8", "value": "11.8" }, { "text": "11.9", "value": "11.9" }, { "text": "12.0", "value": "12.0" }, { "text": "12.1", "value": "12.1" }, { "text": "12.2", "value": "12.2" }, { "text": "12.3", "value": "12.3" }, { "text": "12.4", "value": "12.4" }, { "text": "12.5", "value": "12.5" }, { "text": "12.6", "value": "12.6" }, { "text": "12.7", "value": "12.7" }, { "text": "12.8", "value": "12.8" }, { "text": "12.9", "value": "12.9" }, { "text": "13.0", "value": "13.0" }, { "text": "13.1", "value": "13.1" }, { "text": "13.2", "value": "13.2" }, { "text": "13.3", "value": "13.3" }, { "text": "13.4", "value": "13.4" }, { "text": "13.5", "value": "13.5" }, { "text": "13.6", "value": "13.6" }, { "text": "13.7", "value": "13.7" }, { "text": "13.8", "value": "13.8" }, { "text": "13.9", "value": "13.9" }, { "text": "14.0", "value": "14.0" }, { "text": "14.1", "value": "14.1" }, { "text": "14.2", "value": "14.2" }, { "text": "14.3", "value": "14.3" }, { "text": "14.4", "value": "14.4" }, { "text": "14.5", "value": "14.5" }, { "text": "14.6", "value": "14.6" }, { "text": "14.7", "value": "14.7" }, { "text": "14.8", "value": "14.8" }, { "text": "14.9", "value": "14.9" }, { "text": "15.0", "value": "15.0" }, { "text": "15.1", "value": "15.1" }, { "text": "15.2", "value": "15.2" }, { "text": "15.3", "value": "15.3" }, { "text": "15.4", "value": "15.4" }, { "text": "15.5", "value": "15.5" }, { "text": "15.6", "value": "15.6" }, { "text": "15.7", "value": "15.7" }, { "text": "15.8", "value": "15.8" }, { "text": "15.9", "value": "15.9" }, { "text": "16.0", "value": "16.0" }, { "text": "16.1", "value": "16.1" }, { "text": "16.2", "value": "16.2" }, { "text": "16.3", "value": "16.3" }, { "text": "16.4", "value": "16.4" }, { "text": "16.5", "value": "16.5" }, { "text": "16.6", "value": "16.6" }, { "text": "16.7", "value": "16.7" }, { "text": "16.8", "value": "16.8" }, { "text": "16.9", "value": "16.9" }, { "text": "17.0", "value": "17.0" }, { "text": "17.1", "value": "17.1" }, { "text": "17.2", "value": "17.2" }, { "text": "17.3", "value": "17.3" }, { "text": "17.4", "value": "17.4" }, { "text": "17.5", "value": "17.5" }, { "text": "17.6", "value": "17.6" }, { "text": "17.7", "value": "17.7" }, { "text": "17.8", "value": "17.8" }, { "text": "17.9", "value": "17.9" }, { "text": "18.0", "value": "18.0" }, { "text": "18.1", "value": "18.1" }, { "text": "18.2", "value": "18.2" }, { "text": "18.3", "value": "18.3" }, { "text": "18.4", "value": "18.4" }, { "text": "18.5", "value": "18.5" }, { "text": "18.6", "value": "18.6" }, { "text": "18.7", "value": "18.7" }, { "text": "18.8", "value": "18.8" }, { "text": "18.9", "value": "18.9" }, { "text": "19.0", "value": "19.0" }, { "text": "19.1", "value": "19.1" }, { "text": "19.2", "value": "19.2" }, { "text": "19.3", "value": "19.3" }, { "text": "19.4", "value": "19.4" }, { "text": "19.5", "value": "19.5" }, { "text": "19.6", "value": "19.6" }, { "text": "19.7", "value": "19.7" }, { "text": "19.8", "value": "19.8" }, { "text": "19.9", "value": "19.9" }, { "text": "20.0", "value": "20.0" }];
const per_time = [{ "text": "00:00", "value": "00:00" }, { "text": "01:00", "value": "01:00" }, { "text": "02:00", "value": "02:00" }, { "text": "03:00", "value": "03:00" }, { "text": "04:00", "value": "04:00" }, { "text": "05:00", "value": "05:00" }, { "text": "06:00", "value": "06:00" }, { "text": "07:00", "value": "07:00" }, { "text": "08:00", "value": "08:00" }, { "text": "09:00", "value": "09:00" }, { "text": "10:00", "value": "10:00" }, { "text": "11:00", "value": "11:00" }, { "text": "12:00", "value": "12:00" }, { "text": "13:00", "value": "13:00" }, { "text": "14:00", "value": "14:00" }, { "text": "15:00", "value": "15:00" }, { "text": "16:00", "value": "16:00" }, { "text": "17:00", "value": "17:00" }, { "text": "18:00", "value": "18:00" }, { "text": "19:00", "value": "19:00" }, { "text": "20:00", "value": "20:00" }, { "text": "21:00", "value": "21:00" }, { "text": "22:00", "value": "22:00" }, { "text": "23:00", "value": "23:00" }];

const per_time_night = per_time.splice(13);

(function (params) {

    Vue.component('vue-admin-settings', {
        props: ['route'],
        template: '#template-admin-settings',

        data: function (params) {
            var profile = app.getProfile();
            return {

                // загрузка
                loading: true,

                // настройки
                day_tariff_start: '',
                night_tariff_start: '',
                day_tariff_points_per_km: null,
                night_tariff_points_per_km: null,
                footer_link_vkontakte: '',
                footer_link_instagram: '',
                footer_link_facebook: '',
                footer_link_email_support: '',
                account_login: localStorage.profile && JSON.parse(localStorage.profile).login,
                account_email: profile.profile.email,
                account_password: '',

                //тарифы он решил выделить до дн яи после
                day_tariff_start_options: per_time,
                night_tariff_start_options: per_time_night,

                day_tariff_points_per_km_options: per_km,
                night_tariff_points_per_km_options: per_km

            }
        },
        beforeCreate() {

            app
                .HTTP({
                    method: 'get',
                    url: 'settings/list',
                    headers: {
                        'Authorization': app.getToken()
                    }
                })
                .then(function (response) {

                    // выставляю настроки, пришедшие с серва
                    response.data.data
                        .forEach(function (item) {
                            this[item.key] = item.value;
                        }.bind(this));

                    this.loading = false;
                    // еще получаю профиль, для того чтобы не хапрашиваеть уже привытнаые данные
                    app
                        .HTTP({
                            method: 'get',
                            url: 'users/profile',
                            headers: {
                                'Authorization': app.getToken()
                            }
                        })
                        .then(function (profile) {
                            localStorage.setItem('profile', JSON.stringify(profile.data.data));
                        })
                        .catch((function (error) {
                            $(this.$el).find('.input').addClass('error');
                            app.clearStor();
                        }).bind(this));

                }.bind(this))
                .catch((function () {
                    localStorage.setItem('token_auth', '');
                    localStorage.setItem('type_page', '');

                    this.loading = false;

                    alert('Ошибка, повторите');
                    location.reload();
                }).bind(this));

        },

        /**
         * Валидация полей
         */
        computed: {
            // валидация логина
            isEmailValid() {
                return app.validator.email(this.account_email);
            },
            //  валидайия поддержки
            isEmailValidSup() {
                return app.validator.email(this.footer_link_email_support);
            },
            isSocialValidvk() {
                return app.validator.social(this.footer_link_vkontakte, 'vk.com');
            },
            isSocialValidfb() {
                return app.validator.social(this.footer_link_facebook, 'fb.com') || app.validator.social(this.footer_link_facebook, 'facebook.com');
            },
            isSocialValidinst() {
                return app.validator.social(this.footer_link_instagram, 'instagram.com');
            },
            isLoginValid() {
                return !(!this.account_login || this.account_login.length < 3 || this.account_login.length > 255)
            },
            isPasswordValid() {
                return this.account_password ? !(this.account_password.length < 3 || this.account_password.length > 255) : true;
            },
        },
        methods: {
            /**
             * Сохарненеи
             */
            save: function () {

                this.loading = true;

                var data = {
                    day_tariff_start: this.day_tariff_start,
                    night_tariff_start: this.night_tariff_start,
                    day_tariff_points_per_km: this.day_tariff_points_per_km,
                    night_tariff_points_per_km: this.night_tariff_points_per_km,
                    footer_link_vkontakte: this.footer_link_vkontakte,
                    footer_link_instagram: this.footer_link_instagram,
                    footer_link_facebook: this.footer_link_facebook,
                    footer_link_email_support: this.footer_link_email_support,
                    // account_login: this.account_login,
                    // account_email: this.account_email,
                };
                if (this.account_password) {
                    data['account_password'] = this.account_password;
                }
                if (this.account_login != (localStorage.profile && JSON.parse(localStorage.profile).login)) {
                    data['account_login'] = this.account_login;
                }
                if (this.account_email != (localStorage.profile && JSON.parse(localStorage.profile).profile.email)) {
                    data['account_email'] = this.account_email;
                }




                app
                    .HTTP({
                        method: 'post',
                        data: data,
                        url: 'settings/save',
                        headers: {
                            'Authorization': app.getToken()
                        }
                    })
                    .then(function (response) {
                        if (data['account_password'] || data['account_login']) {
                            alert('Вы поменяли доступы, надо перелогиниться');
                            profileUpdate();
                            app.clearStor();

                        } else {
                            profileUpdate();
                            alert('Настройки изменены!');
                        }
                        this.loading = false;


                        function profileUpdate() {
                            // еще получаю профиль, обновляю данные
                            app
                                .HTTP({
                                    method: 'get',
                                    url: 'users/profile',
                                    headers: {
                                        'Authorization': app.getToken()
                                    }
                                })
                                .then(function (profile) {
                                    localStorage.setItem('profile', JSON.stringify(profile.data.data));
                                });
                        }
                    }.bind(this))
                    .catch((function (error) {
                        app.clearStor();
                        this.loading = false;
                    }).bind(this));
            }
        }
    });
})();