
(function (params) {


	Vue.component('vue-admin-history', {
		template: '#template-admin-history',
		props: ['route'],
		created() {
			// /history/list
			this.getAdminHistory();
		},
		data: function () {
			return {
				'loading': true,
				'history_data': [],
				'page': 0,
				'updated': null
			}
		},
		beforeDestroy() {
			app.$event
				// добавление айтемов верификации
				// шлется из окна верификации - статус фотографии
				.$off('on-verification-status-change', this.onVerificationStatusChange)
				// отстрел сокета - новое фото
				.$off('on-verification-add', this.onItemAdd);
		},
		mounted() {
			app.$event
				// добавление айтемов верификации
				// шлется из окна верификации - статус фотографии
				.$on('on-verification-status-change', this.onVerificationStatusChange)
				// отстрел сокета - новое фото
				.$on('on-verification-add', this.onItemAdd);
		},
		methods: {
			/**
			 * Добавлю новое фото
			 */
			'onItemAdd': function (data) {
				if (data.data.type == "verification_photo") {
					this.history_data = [].concat([data.data.data], this.history_data);
				}
			},

			/**
			 * Обноыляю статус верификации, 
			 * после отстрела с окна верификации
			 */
			'onVerificationStatusChange': function (currentVerification) {
				let
					index = -1,
					historyUpdateItem = this.history_data.filter(function (item, ind) {
						if (index < 0 && this.history_data[ind].data.verification) {
							index = ind;
							return this.history_data[ind].data.verification.id == currentVerification.id;
						};
					}.bind(this));
				if (index >= 0) {
					historyUpdateItem = Object.assign({}, historyUpdateItem[0]);
					historyUpdateItem.data.verification.photo.status = currentVerification.status;
					this.$set(this.history_data, index, historyUpdateItem);
				}
			},
			/**
			 * Сколлинг и подгрузк новых
			 */
			'onScoroller': function () {
				let
					$elList = $('.history-list'),
					$scrolTop = $elList.scrollTop(),
					$height = $elList.height(),
					$heightInner = $elList.find('>div').height(),
					$scrollHegiht = $heightInner - ($scrolTop + $height),
					isLoad = $scrollHegiht <= 10;

				// подгружаем еще
				if (isLoad && !this.isLoad) {
					// если ноль = то  гооврю 0 и плюсую дальше
					if (!this.page) {
						this.page = 0;
					}
					this.loading = true;
					// илбо прото рлюсю
					this.page++;
					this.isLoad = true;
					this.getAdminHistory(this.page);
				}
			},

			/**
			 * Админская история
			 */
			'getAdminHistory': function (page) {
				let
					xhr = app.HTTP,
					xhrHeaders = { 'Authorization': app.getToken() },
					url = ['/history/list' + (page ? ('/' + page) : '')].join(''),
					xhrOptions = {
						method: 'get',
						url: url,
						headers: xhrHeaders
					};

				// загружаю фотоку
				xhr(xhrOptions)
					.then((data) => {
						this.history_data = [].concat([], this.history_data, data.data.data);
						this.page = page;
						this.loading = false;
						this.isLoad = false;
					})
					.catch((data) => {
						this.loading = false;
						this.isLoad = false;
					});
			}
		}
	});

})();