
(function (params) {
    /**
     * Дашборд.
     *
     */
    Vue.component('vue-admin-dashboard', {
        template: '#template-admin-dashboard',
        props: ['table_row_items', 'route'],
        data() {
            return {

                // загрузка
                loading: true,

                selectedIds: [],
                selectedCount: 0,
                // количество водителей
                drivers_count: 0,
                drivers_type_active: 0,
                empty: false
            }
        },
        created() {

            // Выбор компании, при заверщении - это переход по новой кмопании
            // надо слать запрос
            app.$event.$on('on-set-company', function (filter) {
                // поля поискаи вот нновости по кругу
                var f = [
                    filter.filter.advt_campaigns,
                    filter.filter.advt_campaign_types,
                    filter.filter.advt_campaign_cities
                ];
                // получаю компании
                f && f && f;
                f[0] && f[1] && f[2] && this.setPageCompany(f);
            }.bind(this));

            // Обновляем счетчик визуальный на элементе поика
            app.$event.$on('on-set-count-table', function (count) {
                this.drivers_count = count;
                this.loading = false;
            }.bind(this));


            app.$event.$on(app.$eventsName.onLoad, (flag) => this.loading = !flag);


        },
        methods: {
            /**
             * Утанавливает переход в компанию
             */
            setPageCompany: function (filters) {
                console.info('Переходим на страницу компании, или создаем компани', filters);
                alert('Переходим на страницу компании, или создаем компани. См консоль с паарметрами');
            }
        }
    });

})();