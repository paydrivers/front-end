(function () {

	var
		centrifuge = new Centrifuge("wss://www.centrifugo.paydrivers.com/connection/websocket"),
		chatUpdater = centrifuge;
	app.socket = chatUpdater;

	chatUpdater
		.subscribe('notifications:updater#manager', function (notify) {
			console.info('---- NOTIFY ----')
			console.info(notify)
			console.info('---- NOTIFY ----')
			// пришло текстовое сообщение
			// ропаю лишнее поле
			// есил это отправил админ - от отрпавляю на доставку
			// есил не одмин - то оптравляю на добавление
			notify && app.$event
				.$emit('on-verification-add', notify);
		});

	chatUpdater
		.subscribe('chat:updater#manager', function (message) {
			console.info('---- MESSAGE ----')
			console.info(message)
			console.info('---- MESSAGE ----')
			// пришло текстовое сообщение
			// ропаю лишнее поле
			// есил это отправил админ - от отрпавляю на доставку
			// есил не одмин - то оптравляю на добавление
			message.data && app.$event
				.$emit(app.$eventsName.onNewMessage, message.data);
		});
	chatUpdater
		.on("error", getJWT);

	chatUpdater.on('connect', function (context) {
		console.log('connect');
	});

	chatUpdater.on('disconnect', function (context) {
		console.log('disconnect');
		getJWT();
	});

	getJWT();

	function getJWT() {
		app
			.HTTP({
				method: 'get',
				url: 'init/websocket',
				headers: {
					'Authorization': app.getToken()
				}
			})
			.then(function (data) {

				// app.vueApp.
				localStorage.setItem('jwt_token', data.data.data.token);
				localStorage.setItem('jwt_token_expires_in', data.data.data.expires_in);

				// устанавливаю ключ
				chatUpdater.setToken(data.data.data.token);
				chatUpdater.connect();

			}.bind(this))
			.catch((function () {

				localStorage.setItem('jwt_token', '');
				localStorage.setItem('jwt_token_expires_in', '');

				// деркаю рекурсией, но пока все хуева
				setTimeout(getJWT, 1200);
			}).bind(this));
	}

})();