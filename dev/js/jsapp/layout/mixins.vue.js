var vueMixins = {};

(function mixins() {




	//   получения списка водил
	vueMixins.getDriverList = {
		methods: {

			// формат таблицы - дата (для каждой строки)
			getFormatDateTime: function (unixtime) {
				return app.getFormatDateTime(new Date(unixtime * 1000));
			},

			// последний текст у водителя в чате
			getDriverLastText: function (driver_item) {
				return '';
				// return driver_item.driver.blocked
				//     ? 'Заблочен'
				//     : (this.driver_selected && this.current_messages_list[driver_item.driver.id] && this.current_messages_list[driver_item.driver.id][0].text)
			},

			// активная влкаддка
			isDriverActive: function (driver_item) {
				return (this.driver_selected && driver_item.user && (this.driver_selected.user.id == driver_item.user.id))
			},

			// получчаю полное имя(для каждой строки)
			getFullName: function (item) {
				return item.driver ? [item.driver.first_name, item.driver.last_name].join(' ') : '';
			},

			// список водил по филтру
			getDriverList: function (filters) {
				// отправляю запрос на получения ттаблицы
				app
					.HTTP({
						method: 'get',
						url: filters,
						headers: {
							'Authorization': app.getToken()
						}
					})
					.then(function (response) {

						// если есть метод -  выполняю
						if (this.getDriverUnreadList) {
							this.getDriverUnreadList();
						}

						//  список водил
						this.driverList = response.data.data.list || response.data.data;

						// количетво записей
						// this.countRecords = response.data.data.number_of_records;
						this.countRecords = response.data.data.pagination ? response.data.data.pagination.number_of_records : 0;

						// загрузка
						// this.loading = false;
						setTimeout(function (params) {
							app.$event.$emit(app.$eventsName.onLoad, true);
						}, 200);


					}.bind(this))
					.catch((function () {

						// this.loading = false;
						app.$event.$emit(app.$eventsName.onLoad, true);

						app.clearStor();

					}).bind(this));

			},

		}
	};


	//  смена пушстейта
	vueMixins.pushStatePageChange = {
		methods: {
			changePage: function (event) {
				var target = event.target;
				app.$router.changeRoute({
					href: target.dataset.href,
					title: target.innerHTML
				});
			}
		}
	};



	// типы сортировки
	let sortArray = [
		'auto_name',
		'auto_guide',
		'auto_number',
		'driver_full_name',
		'driver_created_at'
	];

	// Миксин для реакции цекобсов, для табов вчастности
	vueMixins.checkboxMixin = {
		created() {

			// сброс чекнутых
			app.$event
				.$on('on-table-checkbox-reset', function (data) {
					this.selectedIds = [];
					this.selectedCount = 0;
				}.bind(this));

			// событие активации чекбоксов
			// принимаю айдшники
			app.$event
				.$on('on-page-table-checkbox', function (ids) {
					// заменяю цифру на выделенной кнопке
					this.selectedIds = ids;
					this.selectedCount = ids.length;
				}.bind(this));
		},
	}
	// Миксин для выделялок 
	// - таблицы
	// - tableSort сотрировка
	// - tableCheckedAll выделение всего
	// - tableCheckedItem выделеление 1 айтема
	vueMixins.tableCheckboxMixin = {
		/**
		* Функции, которые
		* будем использовать.
		*/
		beforeMount() {

			// событие активации чекбоксов
			// принимаю айдшники
			this
				.$off()
				.$on('on-page-table-checkbox', function (ids) {
					// заменяю цифру на выделенной кнопке
					this.selectedIds = ids;
					this.selectedCount = ids.length;
				}.bind(this));

		},

		methods: {
			// сортировка
			tableSort: function (event) {

				this.tableSortArray = this.tableSortArray.map(function (item) {
					if (item.positions) {
						delete item.positions;
					}
					return item;
				});

				var
					columnName = $(event.target).closest('[data-column]').data('column'),
					current = this.sortarrows[columnName];

				var newCurrent = '';
				switch (current) {
					case 'asc':
						newCurrent = 'desc';
						break;
					case 'desc':
						newCurrent = '';
						break;
					case '':
						newCurrent = 'asc';
						break;
				}

				// меняю визуально
				this.sortarrows[columnName] = newCurrent;

				//нахожу элемент в сортировке
				currentSort = this.sort.filter(function (item) {
					if (item.name === columnName) {
						return true;
					}
				})[0];

				// нашли, установлили новую позициню
				currentSort.direction = newCurrent;

				let
					isCompare = false,
					indexSlice = 0,
					tableItem = this.tableSortArray.filter(function (item) {
						return (!isCompare && item.name === columnName) && (isCompare = true) || (!isCompare && indexSlice++);
					});

				this.tableSortArray.splice(indexSlice, 1)

				if (currentSort.direction != '') {
					this.tableSortArray.push(currentSort);
				}

				// выставляем сотрировку
				this.tableSortArray = this.tableSortArray.map(function (item, key) {
					item.position = parseInt(key, 10) + 1;
					return item;
				});

				this.setSort();

			},
			/**
			 * выделение или подно снятие чекбоксов со строк
			 * эмулирует клик на строку
			 */
			tableCheckedAll: function (event) {
				var $table = $(this.$el);
				var currentValue = event.target.checked;

				$table
					.find('.table-item :checkbox')
					.filter(function () {
						return currentValue ? !this.checked : this.checked;
					})
					.closest('.table__string')
					.click();

			},

			/**
			 * Клик по строке, включет чекбок, 
			 * запимсывает выделенные айтемы, либо вырезает из массива
			 * @param id - айди записи
			 */
			tableCheckedItem: function (id) {
				let indexSelected = this.selectedIds.indexOf(id);

				if (indexSelected + 1) {

					// если есть -то вырезаю айтем из массива
					this.selectedIds.splice(indexSelected, 1);
				} else {

					// если нет такого в массиве то пушу его туда
					this.selectedIds.push(id);
				}

				// шлю событие о том что чекбоксы чекнулись, 
				// передавая id
				app.$event.$emit('on-page-table-checkbox', this.selectedIds);
			}

		},
		watch: {
			// следим за измнением мета-данных ответа, от количесвте водителей
			countRecords: function () {
				app.$event.$emit('on-set-count-table', this.countRecords);
			}
		}
	};

	// ммиксин для вотчера выбора у админов
	vueMixins.watchFilterMixin = {

		// при создании - подписыываюсь на событие измеения слайдеров в фильтре
		// записываю в переменные их значения
		created() {
			app.$event
				// событие для сброса селекта в поиске
				.$on(app.$eventsName.onSliderInput, (function (data) {
					if (data.name === 'carage' || data.name === 'driverage') {
						this[data.name === 'carage' ? 'auto_year' : 'driver_age'] = data.values;
					}
				}).bind(this));

		},
		watch: {
			// выбор марки
			auto_marks: function () {
				this.setFilter('auto_marks');
			},
			// выбор модели
			auto_models: function () {
				this.setFilter('auto_models');
			},
			// Выбор кузова
			auto_series: function () {
				this.setFilter('auto_series');
			},
			// выбор цвета
			auto_colors: function () {
				this.setFilter('auto_colors');
			},
			// год выпуска
			auto_year: function () {
				this.setFilter('auto_year');
			},
			// год рждения водителю
			driver_age: function () {
				this.setFilter('driver_age');
			},
			// текстовой поиска
			q: function () {
				this.setFilter('q');
			}
		}
	};


})();
