(function () {

    /**
      * Комопнент для авторизации
      */
    Vue.component('app-auth', {

        // тип модульного окна
        props: ['modalType'],

        mixins: [vueMixins.pushStatePageChange],

        data: function () {
            return {
                loginText: '',
                passwordText: '',
                recoveryEmail: ''
            }
        },

        template: '#template-modal',

        // если авторизация подключена - показываем модалку
        mounted() {
            $(this.$el)
                .modal(ModalSettings);
        },

        // перед удалением - скрываю модалку
        beforeDestroy() {
            $(this.$el)
                .modal('hide');
        },

        /**
         * Наблюдаю за роутом, который 
         * меняется ниже - в глобальном событии на `window`
         */
        watch: {
            modalType: function () {
                $(this.$el)
                    .modal('hide')
                    .modal(ModalSettings);
            }
        },
        computed: {
            // валидация мыла
            isEmailValid() {
                return app.validator.email(this.recoveryEmail);
            }
        },
        methods: {

            /**
             * Гоу на авторизацию
             */
            goAuth: function () {
                app.$router.changeRoute({
                    href: '/auth/login',
                    title: 'Авторизация'
                });

            },

            goMess: function () {
                app.$router.changeRoute({
                    href: '/auth/message',
                    title: 'Восстановили'
                });
            },

            /**
             * Восстановление пароля
             */
            recovery: function () {
                app
                    .HTTP({
                        method: 'post',
                        url: 'users/password/reset',
                        data: {
                            email: this.recoveryEmail
                        },
                        headers: {
                            'Content-Type': 'application/json',
                        }
                    })
                    .then(this.goMess.bind(this))
                    .catch((function (error) {
                        $(this.$el).find('.input').addClass('error');
                        alert('Ошибка входа, либо неверные доступы. Повторите');
                        app.clearStor();
                    }).bind(this));
            },
            logIn: function () {

                app
                    .HTTP({
                        method: 'post',
                        url: 'auth/login/password',
                        data: {
                            "login": this.loginText,
                            "password": this.passwordText
                        },
                        headers: {
                            'Content-Type': 'application/json',
                        }
                    })
                    .then(login)
                    .catch((function (error) {
                        $(this.$el).find('.input').addClass('error');
                        alert('Ошибка входа, либо неверные доступы. Повторите');
                    }).bind(this));


                function login(response) {

                    var page = '';
                    var firstPage = '';

                    if (response.data.data.role == 'customer' || response.data.data.role == 'administrator') {

                        // если этороль клиента = то клиент
                        if (response.data.data.role == 'customer') {
                            page = 'client';
                            firstPage = page + '';
                        } else {
                            // если этороль админ = то админ и дпшборж
                            page = 'admin';
                            firstPage = page + '/' + (localStorage.getItem('lastRoute') || 'dashboard');
                        }


                        localStorage.setItem('token_auth', JSON.stringify(response.data.data.access_token));
                        localStorage.setItem('type_page', page);

                        // еще получаю профиль, для того чтобы не хапрашиваеть уже привытнаые данные
                        app
                            .HTTP({
                                method: 'get',
                                url: 'users/profile',
                                headers: {
                                    'Authorization': app.getToken()
                                }
                            })
                            .then(function (profile) {
                                localStorage.setItem('profile', JSON.stringify(profile.data.data));
                                // локейшн меняю
                                app.$router.changeRoute({
                                    href: '/' + firstPage,
                                    title: 'Главная'
                                });

                                location.reload();
                            })
                            .catch((function (error) {
                                $(this.$el).find('.input').addClass('error');
                                app.clearStor();
                            }).bind(this));


                    }

                    // зануляю
                    this.loginText = '';
                    this.passwordText = '';

                    console.info(response)
                }
            }
        }
    });
})();