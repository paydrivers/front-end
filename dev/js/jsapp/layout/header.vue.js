(function (params) {
	Vue.component('vue-header', {
		props: [
			'route',
			// количества сообзщений
			'unread_messages_total',
			// количество нотификаций
			'notification_count',
			// балланс смс
			'balance_sms'
		],
		template: '#template-header',
		mixins: [vueMixins.pushStatePageChange],
		data: function (params) {
			return {
				// им профиля амина
				'profilename': app.getProfileName()
			}
		},
		methods: {
			//нотификация
			openNotificationPopup: function () {
				app.$event.$emit('on-notify-open', true);
			},

			logout: function () {
				app.HTTP({
					method: 'post',
					url: 'auth/logout',
					headers: {
						'Authorization': app.getToken()
					}
				})
					.then(app.clearStor)
					.catch(app.clearStor);

			}
		}
	});

})();