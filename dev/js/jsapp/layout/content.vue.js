(function (params) {



	/**
	 * Комопнент вью - контент
	 */
	Vue.component('app-content', {
		template: '#template-content',

		mixins: [vueMixins.pushStatePageChange],

		props: [
			// тип страницы
			'type',
			// роут
			'route',
			// вери
			'verification',
			// текущий спиоск 
			'current_messages_list',
			// список сообщений по водилам
			'unread_messages_list',
			// не прочтеные сообщения
			'unread_messages_total'
		]



	});


})();