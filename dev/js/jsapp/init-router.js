"use strict";

// типы роутов авторизации

// типы роутов авторизации
const AuthTypes = app.AuthTypes;
//тип рроутов для админки с клиента
const RouteTypes = {
	admin: app.RoutesConst,
	client: []
}

// кнопка эскейапа
const ESCKey = 27;

const ModalSettings = {
	backdrop: 'static',
	keyboard: false
};


(function () {
	// запомнию ссылку на рроутер-фн
	var $router = app.$router;

	$appCloseItem = appCloseItem

	/**
	 * Создем основноый компонент-вью
	 */
	app.vueApp = new Vue({
		el: '#app',

		props: [],

		data: function () {
			return {

				// признак того что сеть есть или нет
				'isOnline': false,

				//линки на футер
				'footer_link_vkontakte': null,
				'footer_link_instagram': null,
				'footer_link_facebook': null,
				'footer_link_email_support': null,

				// страница загружена или не загружена - флаг
				'pageLoadIsFull': false,

				// опция для роута. 
				// тут будет деражаться href
				'currentRoute': $router.getRouteHref(),
				// опция 
				'typeAuthRoute': $router.getRouteAuth(),
				'isAuth': $router.getSession(),


				'get_date_format_fn': app.getFormatDateTime,

				// балланс смс
				'balance_sms': 0,


				// текущий откртыый список сообщений
				'current_messages_list': [],
				// нпрочитаннаых всего
				'unread_messages_total': 0,
				// непрочитанных 
				'unread_messages_list': [],

				// верификация
				'verification': null,

				// показан или скрыт попап с нотификацией
				'is_nofify_popup': false,
				// нотификации
				'unchecked_notify': {},

			}
		},

		created() {

			// получаю нотификацию
			this.debounceUnread = {
				msg: jQuery.debounce(getDriverUnreadList, 800).bind(this),
				notify: jQuery.debounce(getNotificationsUnprocesses, 300).bind(this),
			}


			app.$event
				// глобавльный эскейп и закрытие
				.$on(app.$eventsName.onGlobalClose, () => this.is_nofify_popup = false)
				// Событие для pushState для роутов
				.$on('on-push-state', _pushState.bind(this))
				// счеттчик вниз или вверх для нотификации
				// @isFalse - флак - вниз считать или вверх
				.$on('on-notify-open', _notificationOpen.bind(this))
				// вырезаю айтем из истории, которые заапрувили или отреджектили
				// при любом событии, т.е. любое событие - удаляет айтем
				.$on('on-verification-status-change', verificationRemoved.bind(this))
				// добавление айтемов верификации
				.$on('on-verification-add', verificationAdd.bind(this))

				// апдейтер счетчиков
				.$on(app.$eventsName.onMessageCounterUpdate, this.debounceUnread.msg)
				.$on(app.$eventsName.onNewMessage, socketNewMessage.bind(this))

				// Отктыиеи верификации
				.$on(app.$eventsName.verificationOpen, _verificationOpen.bind(this));






			// проверяю на авторизашку
			if (!localStorage.getItem('token_auth')) {
				// записываю текущий роунт
				this.typeAuthRoute = $router.getRouteAuth();
				return false;
			}
			Offline.options = app.checkOnlineOptions;
			Offline.on('confirmed-down', function () {
				this.isOnline = false;
			}.bind(this));
			Offline.on('confirmed-up', function () {
				this.isOnline = true;
			}.bind(this));




			this.debounceUnread.msg();
			this.debounceUnread.notify();



			// до боди нам не добраться, а слушать надо глобально.
			// объявляем, биндим туда сразу параметр
			document.addEventListener('keyup', (e) => {
				switch (e.keyCode) {
					case ESCKey: this.appCloseItem.call(this, 'esc'); break;
				}
			});

			// Событие на смену состояния истории. 
			// Меняю - кидаю новый роут
			window.addEventListener('popstate', () => {
				app.vueApp.currentRoute = $router.getRouteHref();
			}, false);


			getBalance.call(this);

			this.getDataFooter()


		},

		// Функции, которые будем использовать.
		methods: {

			getDataFooter: function name(params) {
				// получаю настройки
				this.checkPage() && app
					.HTTP({
						method: 'get',
						url: 'settings/list',
						headers: {
							'Authorization': app.getToken()
						}
					})
					.then(function (response) {
						response
							.data
							.data
							.splice(4)
							.forEach((name) => {
								this[name.key] = name.value;
							});
						this.pageLoadIsFull = true;
					}.bind(this));
			},


			/**
			 * Клик по общему врепперу
			 * @param isClickOrType - тип клика - мышка
			 */
			appCloseItem: $appCloseItem,

			/**
			 * Проверка роутов на правду
			 * Список роутов берется с верху страницы
			 * Остлаьные - просто возращат обратно
			 */
			checkPage: checkPage
		},

		watch: {

			/**
			 * Наблюдаю за роутом, который 
			 * меняется ниже - в глобальном событии на `window`
			 */
			currentRoute: $watchCurrentRoute
		}
	});

	/**
	 * Открытие верификации
	 * @param {*} verification 
	 */
	function _verificationOpen(verification) {

		// закрываем верификацию
		if (!verification) {
			this.verification = null;
			return false;
		}

		// из истории
		if (verification.polymorph_id) {
			this.verification = {
				driver: verification.data
			};
		} else {
			// если это просто открытие верификации
			this.verification = verification;
		}
	}




	/**
	 * Пуштейт - запись истории
	 * @param {*} target 
	 */
	function _pushState(target) {
		this.currentRoute = $router.getRouteHref();
		// если эьто не ауутентификайция, то записываю роут, который последний
		var currRouteSplit = this.currentRoute.split('/');
		(!(this.currentRoute.indexOf('auth') + 1)) && localStorage.setItem('lastRoute', currRouteSplit[currRouteSplit.length - 1]);
	}

	/**
	 * Открытие нотификаци
	 */
	function _notificationOpen() {
		if (this.unchecked_notify.count && this.unchecked_notify.last_history && this.unchecked_notify.last_history.length) {
			this.is_nofify_popup = true;
		} else {
			getNotificationsUnprocesses.call(this);
			this.is_nofify_popup = true;
		}
	}


	/**
	 * Редактирование
	 * @param {*} currentVerification 
	 */
	function verificationRemoved(currentVerification) {
		index = -1;
		let cloneNoty = [].concat([], this.unchecked_notify.last_history),
			count = this.unchecked_notify.count;
		cloneNoty.forEach(function (ve, indx) {
			if (ve.type === 'verification_photo' &&
				(ve.data.verification && ve.data.verification.id == currentVerification.id || ve.data.data && ve.data.data.verification.id == currentVerification.id)) {
				cloneNoty[indx] = null;
				count--;
			}
		}.bind(this));
		// // вырезаю, если был найдет накой
		// if (index >= 0) {
		// 	cloneNoty.splice(index, 1);
		// меняю историю
		count = count < 0 ? 0 : count;
		cloneNoty = cloneNoty.map(function (item) {
			return item ? item : false;
		})


		this.unchecked_notify = {
			count: count,
			last_history: cloneNoty
		}
		// }
		// вырезать еще надо из сообщений, точнее заменить
		console.log('--- Verification Removed');
	}

	/**
	 * Добвление верификации
	 * @param {*} data 
	 */
	function verificationAdd(data) {
		if (data.data.type == "verification_photo") {
			let cloneNoty = [].concat([], this.unchecked_notify.last_history);
			// получаю историю
			cloneNoty.push(data.data);
			// считаю сечтчик
			this.unchecked_notify = {
				count: this.unchecked_notify.count + 1,
				last_history: cloneNoty
			}
			console.log('--- Verification Add', cloneNoty)
		}
	}

	/**
	 * Получение балланса
	 */
	function getBalance() {
		app
			.HTTP({
				method: 'get',
				url: 'sms/balance',
				headers: {
					'Authorization': app.getToken()
				}
			})
			.then(function (response) {

				this.balance_sms = response.data.data.cash;
			}.bind(this));
		// .catch(app.clearStor);
	}


	function socketNewNotify(ntf) {
		console.log(ntf)
	}

	/**
	 * Метод обработки получения сокета для сообщений
	 * @param {*} msg 
	 */
	function socketNewMessage(msg) {
		let isChat = app.$router.getRouteHref().indexOf('chat') + 1 > 0;
		// шлю сообщение, если открыт канал с чатом, 
		//или говрю о том что надо обьновиить счетчики, если открыт чат
		app.$event.$emit(isChat ? app.$eventsName.onAddChatMessage : app.$eventsName.onMessageCounterUpdate, msg);
	}

	/**
	 * Закрыватель
	 * @param {*} isClickOrType 
	 */
	function appCloseItem(isClickOrType) {

		// если это мышка (true), то - надо обрабатывать только для того 
		// чтобы чтот закрыть, типа дропдауна, на что и проверяем
		// чтобы не эммитилось кучу раз
		switch (isClickOrType) {
			case true:
				// Шлем событие о том, что надо селекты сбросить - закрыть
				document.querySelector('.slider-select-itaem-popup.active') && app.$event.$emit(app.$eventsName.onGlobalClose);
				break;
			case 'esc':
				// Шлем событие о том, что надо селекты сбросить - закрыть
				app.$event.$emit(app.$eventsName.onGlobalClose);
				break;
		}

		this.is_nofify_popup = null;

		return isClickOrType;

	}
	/**
	 * 
	 */
	function checkPage() {

		// режу роут, получаю из стореджа тип странцы
		var route = window.location.pathname.split('/').splice(1);

		// достаю из стореджа все что у меня есть по авторизации
		var settedTypePage = localStorage.getItem('type_page');
		var settedToken = localStorage.getItem('token_auth');

		// если тип роута совпадает с тем что в сессии - то все оке
		// а так же сравниваю с роутами, что прописаны выше
		if (settedToken && settedTypePage === route[0] && RouteTypes[route[0]].indexOf(route[1]) + 1) {
			return true;  // если все ок - то пропускаю
		} else {
			// если есть тип страницы
			if (settedTypePage && settedToken) {
				// если у нас не сопвпадают роуты или не тот тип роуита, то 
				//  перенаправлю на нужный тип и перехагружю страницу
				app.$router.changeRoute({
					href: '/' + settedTypePage + '/' + RouteTypes[settedTypePage][0],
					title: 'Админка'
				})
				location.reload();
			}
		}
	}


	function $watchCurrentRoute() {

		// проверяю сессию
		// если нет сессии - то обновляю роут до авторизации
		let isAuth = this.isAuth;
		if (!isAuth) {
			app.vueApp.typeAuthRoute = $router.getRouteAuth();
			return;
		}

		// если авторизация прошла успешно
		// сменили роут - то чекаем, верный ли
		if (this.checkPage()) {
			this.typeAuthRoute = window.location.pathname.split('/').splice(1);
		}
	}


	/**
	 * Получю список непрочтенных
	 * записываю их в unread_messages_list
	 */
	function getDriverUnreadList() {
		app
			.HTTP({
				method: 'get',
				url: 'chat/messages/unread/total',
				headers: {
					'Authorization': app.getToken()
				}
			})
			.then(function (data) {
				let
					res = data && data.data.data,
					by_drivers = res && res.by_users,
					unread_messages_list = {};

				// записываю для количетсв под id водилы
				by_drivers && by_drivers.forEach((itm) => {
					unread_messages_list[itm.id] = itm.total;
				})

				// общее окличетсва
				this.unread_messages_total = res.total;

				// запоминаю список
				this.unread_messages_list = unread_messages_list;

			}.bind(this));
	}

	/**
	 * Получю список непрочтенных
	 * записываю их в unread_messages_list
	 */
	function getNotificationsUnprocesses() {
		app
			.HTTP({
				method: 'get',
				url: 'history/notifications/unprocessed',
				headers: {
					'Authorization': app.getToken()
				}
			})
			.then(function (data) {
				this.unchecked_notify = {};
				let unchecked_notify = [];
				// переформировываю масив истории
				data && data.data.data.last_history
					.forEach(function (item) {
						if (item.type != "verification") {
							unchecked_notify.push(item);
						} else {
							data.data.data.count--;
						}
					});
				this.unchecked_notify.count = data.data.data.count
				this.unchecked_notify.last_history = unchecked_notify
			}.bind(this));
	}
	$(function () {
		$('body').on('click', function () {
			(
				$('.slider-select-item-popup.active').length
				|| $('.notify-popup.opened').length
			)
				&& $appCloseItem(true);
		})
	})

})();
