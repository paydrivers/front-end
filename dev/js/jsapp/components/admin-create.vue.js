(function (params) {

    $fnEmmitF = $.debounce($emmitF, 250);

    Vue.component('vue-admin-create', {
        template: '#template-admin-create',
        prop: ['rft'],
        data: function () {
            return {
                // выбрираю регинон
                filterCOptions: [{ id: '', text: 'Не выбрано' }],
                advt_campaign_cities: '',
                filterCDisbled: true,

                // рекламный носитель
                filterRNOptions: [{ id: '', text: 'Не выбрано' }],
                advt_campaign_types: '',
                filterRNDisbled: true,

                // рекламная компания
                filterRKOptions: [{ id: '', text: 'Не выбрано' }],
                advt_campaigns: '',
                filterRKDisbled: false
            }
        },

        mounted() {

            this.filterRKOptions = [{ id: '', text: 'Не выбрано' }];
            app
                .HTTP({
                    method: 'get',
                    url: '/a/campaigns',
                    headers: {
                        'Authorization': app.getToken()
                    }
                })
                .then(function (response) {
                    response.data.data
                        .forEach(function (option, index) {
                            this.filterRKOptions.push({
                                'id': option.advt_campaign_id,
                                'text': option.advt_campaign_name,
                            })
                        }.bind(this));
                    this.filterRKOptions.push({
                        'id': 999999,
                        'text': 'PayDrivers',
                    });
                    // this.filterRNDisbled = false;
                }.bind(this))
        },
        methods: {
            setFilter: function (filterName, value) {

                // срасываю фильтр
                this.filter = [];

                // записываю запии в фильтр для передачи
                [
                    'advt_campaigns',
                    'advt_campaign_types',
                    'advt_campaign_cities'
                ]
                    .forEach((value) => {
                        if (this[value] && !!this[value][0]) {
                            this.filter[value] = this[value];
                        }
                    });

                // вызываю установку селектов
                switch (filterName) {
                    case 'advt_campaigns':
                        getSearchRN.call(this, value);
                        break;
                    case 'advt_campaign_types':
                        getSearchC.call(this, value);
                        break;
                }

                //даю понять что фильтры установлены, через функицю с дебоунс
                $fnEmmitF({
                    name: filterName,
                    filter: this.filter
                });
            }
        },
        watch: {

            // Келкамная компания
            advt_campaigns: function (value) {
                this.setFilter('advt_campaigns', value);
            },

            // Реклмный носитель, фильтр сверху
            advt_campaign_types: function (value) {
                this.setFilter('advt_campaign_types', value);
            },

            // Город рекламной компании
            advt_campaign_cities: function (value) {
                this.setFilter('advt_campaign_cities', value);
            }
        }
    });

    /**
      * Дебонс для устанвоки фильтра
      * @param {*} f  - функиця
      */
    function $emmitF(f) {
        app.$event.$emit('on-set-company', f);
    }

    /**
     * Получаю список рекламных компаний
     */
    function getSearchRN(value) {

        // сначал чищу все что установлено
        this.filterRNOptions = [{ id: '', text: 'Не выбрано' }];
        // this.filterCOptions = [{ id: '', text: 'Не выбрано' }];

        this.filterRNDisbled = true;

        this.advt_campaign_types = '';
        this.advt_campaign_cities = '';

        // если нет значения - то личщу и значения
        if (!value) {
            return false;
        }

        app
            .HTTP({
                method: 'get',
                url: 'a/campaigns/' + value + '/types/list',
                headers: {
                    'Authorization': app.getToken()
                }
            })
            .then(function (response) {
                response.data.data
                    .forEach(function (option, index) {
                        this.filterRNOptions.push({
                            'id': option.advt_campaign_id,
                            'text': option.advt_campaign_name,
                        })
                    }.bind(this));

                // если выбрана рекламная компания "Гонка"
                if (this.advt_campaigns == 999999) {
                    this.filterRNOptions.push({
                        'id': 999999,
                        'text': 'Гонка',
                    });
                }

                this.filterRNDisbled = false;
            }.bind(this))
            .catch(getSearchError.bind(this));
    }


    /**
     * Получаю список городов
     * 
     */
    function getSearchC(value) {
        this.filterCOptions = [{
            id: '',
            text: 'Не выбрано'
        }];
        this.advt_campaign_cities = '';
        this.filterCDisbled = true;
        if (!value) {
            return false;
        }
        app
            .HTTP({
                method: 'get',
                url: 'a/campaigns/' + this.advt_campaigns + '/types/' + value + '/cities/list',
                headers: {
                    'Authorization': app.getToken()
                }
            })
            .then(function (response) {
                response.data.data
                    .forEach(function (option, index) {
                        this.filterCOptions.push({
                            'id': option.id,
                            'text': option.name,
                        })
                    }.bind(this));
                this.filterCDisbled = false;
            }.bind(this))
            .catch(getSearchError.bind(this));
    }

    function getSearchError(error) {
        console.info('--- log : ');
        console.warn(error);
    }
})();