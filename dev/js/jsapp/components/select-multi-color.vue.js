(function (params) {

    /**
     * Мульти селект
     * @param {*} state 
     */
    Vue.component('vue-select-multi-color', {
        template: '#template-select-multi-color',
        props: ['options', 'value', 'placeholder', 'disabled'],
        /**
         * Dom Ready
         * @param {*} state 
         */
        mounted: function () {
            var self = this;
            $(this.$el).find('select')
                .select2({
                    placeholder: 'Все цвета',
                    language: 'ru',
                    // closeOnSelect: false,
                    templateSelection: formatState,
                    templateResult: formatState,
                    dropdownCssClass: 'select-2-color--drop'
                }).on('change', function (e) {
                    self.setValue();
                });;
        },
        methods: {
            setValue() {
                values = $(this.$el).find('select').val();
                values = values.map(function (item) {
                    return Number(item);
                })
                this.$emit('input', values);
            }
        }


    });

    /**
    * Стилизация вывода мультиселета
    * @param {*} state 
    */
    function formatState(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<span><b class="select-2-color" style="background: ' + $(state.element).data('select2-color') + '"><b class="select-2-checkbox color"></b></b><span class="checkbox-color">' + state.text + '<span></span>'
        );
        return $state;
    }
})();