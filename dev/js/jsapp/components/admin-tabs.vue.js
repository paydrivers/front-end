/**
 * Комопнент табов
 * 
 * - перещелкивание табов
 * - отправка сообщения юзерам
 * - изменить коэффициент юзерам
 * - забанить юзеров
 * - скачать фотки юзеров
 */
(function (params) {
	Vue.component('vue-admin-tabs', {
		template: '#template-admin-tabs',
		mixins: [vueMixins.checkboxMixin],
		//active_tab - активный таб таблицы(бан 1, не бан 0)
		props: ['drivers_type_active', 'active_tab'],

		data: function () {
			return {
				//индекс активного таба
				tabIndex: this.active_tab || null,

				// текст модели текстареи
				textMessage: '',

				// текст модели для причины бана
				banMessage: '',

				// выделеные строки таблицы
				selectedIds: [],

				// колическо выделенных айтемов
				selectedCount: 0,
			}
		},

		computed: {
			// Это пейдж, черная рабоа - для черного первая из списка
			is_blocked_list: function () {
				return this.drivers_type_active === 1;
			},
			// текст на кнопках - БЛОК
			blocked_button_text: function () {
				return (this.is_blocked_list ? 'Разблокировать' : 'Заблокировать') + (' доступ ' + this.selectedCount + app.declOfNum(this.selectedCount, [' водителю', ' водителям', ' водителям']))
			},
			// текст на кнопках - КОЭФ
			coeff_button_text: function () {
				return ('Изменить коэффициент ' + this.selectedCount + app.declOfNum(this.selectedCount, [' водителю', ' водителям', ' водителям']))
			},
			// текст на кнопках - МЕССАГ
			mess_button_text: function () {
				return ('Отправить сообщение ' + this.selectedCount + app.declOfNum(this.selectedCount, [' водителю', ' водителям', ' водителям']))
			},
			// текст- агитатат
			blocked_text: function () {
				return !this.drivers_type_active ? 'Отключить водителя' : 'Разбанить водителя';
			},
			// стли неактивный там
			is_active_tabs_css: function () {
				var drivers_type_active = !this.selectedCount;
				return {
					'opacity': (drivers_type_active ? .2 : 1),
					'cursor': (!drivers_type_active ? '' : 'default')
				}
			}
		},

		methods: {

			beforeCreate: function () {
				this.tabChange();
			},

			/**
			 * Метод для переключения табов
			 * @param index - индекс таба
			 */
			tabChange: function (index) {
				// если нет выделеных - то нахуй
				if (!this.selectedCount) {
					return false;
				}

				// установка дефотла
				$(this.$el).find('#coeeff .noUi-target')[0].noUiSlider.set(app.defCoeff);

				// индекс 1 и 2 - это "бан"
				if (index == 2) {

					// если 1 индекс,  то это отправляем в бан
					this['downloadPhohos']();
				} else {

					// иначе меням индекс и переключаем вкладку
					this.tabIndex = index;
				}
			},


			/**
			 * Изменение коэффциента
			 */
			changeCoefficient: function () {
				var rate = $(this.$el).find('#coeeff .noUi-target')[0].noUiSlider.get();

				app.$event.$emit(app.$eventsName.onLoad, false);

				app
					.HTTP({
						method: 'post',
						url: 'drivers/rates',
						headers: {
							'Authorization': app.getToken()
						},
						data: {
							users_ids: this.selectedIds,
							rate: rate
						}
					})
					.then(function (response) {
						// дергаем чтобы изменить коэффициента
						app.$event.$emit(app.$eventsName.onLoad, true);
						app.$event.$emit(app.$eventsName.onUpdate, { type: 'coeff', selectedKeys: this.selectedIds, value: rate });
						$(this.$el).find('#coeeff .noUi-target')[0].noUiSlider.set(app.defCoeff);
						alert('Успех изменения коэффициента! Значение слайдера восстановлено')
					}.bind(this))
					.catch((function () {
						app.$event.$emit(app.$eventsName.onLoad, true);
						$(this.$el).find('#coeeff .noUi-target')[0].noUiSlider.set(app.defCoeff);
						alert('Ошибка изменения коэффициента!')
					}).bind(this));
			},


			unbannedUser: function () {
				// конфирм, для подверждения
				app.$event.$emit(app.$eventsName.onLoad, false);

				// конфирм, для подверждения
				app.bunController({
					state: false,
					users_ids: this.selectedIds,
					reason: this.banMessage || 'без причины'
				})
					.then(function (response) {
						// alert('Водители  разблокированы!');
						// очищаем  поле ввода
						app.$event.$emit(app.$eventsName.onUpdate, { type: 'blocked', selectedKeys: this.selectedIds });
						app.$event.$emit(app.$eventsName.onLoad, true);
						this.banMessage = '';
					}.bind(this))
					.catch((function () {
						alert('Ошибка бана. Повторите');
						app.$event.$emit(app.$eventsName.onLoad, true);
					}).bind(this));
			},

			/**
			 * Бан юзера.
			 * Спрашиваем о том, надо ли, и если надо - то баним
			 */
			bannedUser: function () {
				app.$event.$emit(app.$eventsName.onLoad, false);

				// конфирм, для подверждения
				app.bunController({
					state: true,
					users_ids: this.selectedIds,
					reason: this.banMessage || 'без причины'
				})
					.then(function (response) {
						alert('Водители забанены!');
						// очищаем  поле ввода
						this.banMessage = '';
						app.$event.$emit(app.$eventsName.onUpdate, { type: 'blocked', selectedKeys: this.selectedIds });
						app.$event.$emit(app.$eventsName.onLoad, true);

					}.bind(this))
					.catch((function () {
						alert('Ошибка бана. Повторите');
						app.$event.$emit(app.$eventsName.onLoad, true);
					}).bind(this));
			},

			/**
			 * Качаю фотографии от пользователей
			 */
			downloadPhohos: function () {
				let
					xhr = app.HTTP,
					xhrHeaders = { 'Authorization': app.getToken() },
					xhrOptions = {
						method: 'post',
						url: 'users/profile/download-attachments',
						headers: xhrHeaders,
						data: {
							users_ids: this.selectedIds
						}
					};

				// загружаю фотоку
				xhr(xhrOptions)
					.then(function (data) {
						data.data.data.forEach(function (item) {
							window.open(item.path, "_blank");
						});
					})

			},

			// отправка соощений
			sendMessages: function (id) {
				app.$event.$emit(app.$eventsName.onLoad, false);

				let
					data = {
						"users": this.selectedIds.map((mapItem) => {
							return {
								"uuid": app.getHash(),
								"id": mapItem
							}
						}),
						"message": this.textMessage
					},

					// хедер для авторизщации
					headersAuth = { 'Authorization': app.getToken() },
					// хшр
					xhr = app.HTTP,
					// опции для запроса
					xhrOptions = {
						method: 'post',
						url: ['chat/messages/simple/send/many'].join(''),
						headers: headersAuth,
						data: data
					};

				// отсылаю сообщение
				xhr(xhrOptions)
					.then((data) => {
						alert('Сообщения высланы');
						this.textMessage = "";
						app.$event.$emit(app.$eventsName.onLoad, true);
					})
					.catch(() => {
						app.$event.$emit(app.$eventsName.onLoad, true);
						alert('Ошибка отправки');
					})
			}
		}
	});
})();