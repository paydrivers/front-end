(function (params) {

    /**
     * Мульти селект
     * @param {*} state 
     */
    Vue.component('vue-select-multi', {
        template: '#template-select-multi',
        props: ['options', 'value', 'disabled'],

        /**
         * Dom Ready
         * @param {*} state 
         */
        mounted: function () {
            var self = this
            $(this.$el)
                .find('select').select2({
                    placeholder: 'Все',
                    language: 'ru',
                    // closeOnSelect: false,
                    templateResult: formatState,
                })
                .on('change', function (e) {
                    self.setValue();
                });
        },

        methods: {
            setValue() {
                values = $(this.$el).find('select').val();
                values = values.map(function (item) {
                    return Number(item)
                })
                this.$emit('input', values);
            }
        }

    });

    /**
    * Стилизация вывода мультиселета
    * @param {*} state 
    */
    function formatState(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<span><b class="select-2-checkbox"></b><span>' + state.text + '<span></span>'
        );
        return $state;
    }
})();