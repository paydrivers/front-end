(function () {

	// Компооент для вывода ОТДЕЛЬНОГО ВОДИТЕЛЯ в списке чата
	// @props is_active - автивный ли
	// @props driver_item - водитель
	// @props driver_name - фулл нейм водителя
	// @props driver_last_text_fn - поледнее соощбениет в чате
	// @props driver_chat_ctrl_flex - метод для события нажатия на водитея 
	Vue.component('vue-admin-chat-driver', {
		'template': '#template-admin-chat-driver',
		'props': [
			'is_active',
			'driver_item',
			'driver_name',
			'last_message',
			'driver_chat_ctrl_flex',
			'unread_messages_list'
		],
		computed: {

			// можно ли открытиывать сообщение с таймером
			dateTime: function () {
				if (!this.driver_item || !this.driver_item.message) {
					return ''
				}
				return app.getFormatDateTime(new Date(this.driver_item.message.created_at * 1000), true)
			},
		}
	});

	// Компооент для вывода водителей в списке чата
	// @props messageList - список сообщений
	// @props driver_selected - выделеный
	// @props get_date_format_fn - метод для формата даты
	// @props driver_messages_list - список сообщний для водителя
	Vue.component('vue-admin-chat-driver-messages-list', {
		'template': '#template-admin-chat-driver-messages-list',
		'props': [
			'driver_selected',
			'get_date_format_fn',
			'driver_messages_list'
		]
	});

	/**
	 * когда мы отправлям сообщение - просихосдит следующее
	 * Сообщение отправляется со статусом 0
	 * 
	 * driver_selected - выбранный водитель
	 * driver_messages_list - водителья сообщения
	 * get_date_format_fn - метод для формата даты
	 */
	Vue.component('vue-admin-chat-message', {
		template: '#template-admin-chat-message',
		props: [
			// для апдейта когда меняется стаитус
			'updated_status',
			// для апдейте, когда меняются данные
			'updated_data',
			// выделенный айтем
			'driver_selected',
			// само сообщение
			'driver_message',
			'updated_verification_expires_in',
			'get_date_format_fn'
		],
		data: function () {
			return {
				// статус доставки сообщения
				'timeRequest': null,
				// время доставки
				'timeDelivery': null,
				'loading': false,
				'textTimer': 'Обработать фото'
			}
		},
		watch: {
			/**
			 * Статус верификации - текст
			 */
			v_status: function () {
				if (this.v_status == 'pause') {
					this.textTimer = 'Обработать фото';
				}
				if (this.v_status == 'success') {
					this.textTimer = 'Подверждено ';
				}
				if (this.v_status == 'reject') {
					this.textTimer = 'Отклонено';
				}
			},

			/**
			 * 
			 */
			expires_in: function () {
				if (this.driver_message.data && this.driver_message.data.verification) {

					clearInterval(this.timeRequest);
					if (this.expires_in) {
						console.log(this.expires_in);

						this.textTimer = app.remainingTime.call(this, this.expires_in * 1000);
						this.timeRequest = setInterval(() => {
							this.textTimer = app.remainingTime.call(this, this.expires_in * 1000);
						}, 1000);
					}

				}
			}
		},

		// updated() {
		// 	if (this.driver_message && this.driver_message.data && this.driver_message.data.verification) {
		// 		console.log(this.driver_message.data.verification.status)
		// 		console.log(this.driver_message.data.verification.expires_in)
		// 	}
		// },

		/**
		 * При создании элемента ДОМ.
		 */
		mounted() {

			console.log(this.expires_in);

			// Если это верификация, то очищаю таймеры, а так же - устанавилваю статус верфификации
			if (this.driver_message && this.driver_message.data && this.driver_message.data.verification) {
				// console.log(this.driver_message.data.verification.status)
				// console.log(this.driver_message.data.verification.expires_in)

				clearInterval(this.timeRequest);
				let verification_msg = this.driver_message.data.verification;

				this.textTimer = app.remainingTime.call(this, this.expires_in * 1000);

				// если это пендтнг или верификация - то это "обработка" и нет опции для таймера
				if ((verification_msg.status == 'pending' || verification_msg.status == 'verification') &&
					!verification_msg.expires_in) {
					this.textTimer = 'Обработать фото';
					return false;
				} else {

					// если естутс - "обработка"
					if (verification_msg.status == 'pending' || verification_msg.status == 'verification') {
						this.textTimer = app.remainingTime.call(this, this.expires_in * 1000);
						this.timeRequest = setInterval(() => {
							this.textTimer = app.remainingTime.call(this, this.expires_in * 1000);
						}, 1000);
					} else if (verification_msg.status == 'success') {
						this.textTimer = 'Подверждено';
					} else if (verification_msg.status == 'reject') {
						this.textTimer = 'Отклонено';
					}
				}
			}
		},

		methods: {
			withdrawCashTrue: function () {
				let
					// адрес урла
					urlFull = 'withdraws/' + this.driver_message.data.withdraw.id + '/payed',
					// хедер для авторизщации
					headersAuth = { 'Authorization': app.getToken() },
					// хшр
					xhr = app.HTTP,
					// опции для запроса
					xhrOptions = {
						method: 'post',
						url: urlFull,
						headers: headersAuth
					};

				// делю подверждение, а потом - перезапрос 
				// для того чтобы поставить статус верны
				xhr(xhrOptions)
					.then(function (res) {

						let
							// адрес урла
							urlFull = 'withdraws/' + this.driver_message.data.withdraw.id,
							// хедер для авторизщации
							headersAuth = { 'Authorization': app.getToken() },
							// хшр
							xhr = app.HTTP,
							// опции для запроса
							xhrOptions = {
								method: 'get',
								url: urlFull,
								headers: headersAuth
							};
						xhr(xhrOptions)
							.then(function (res) {
								if (res && res.data && res.data.data) {
									this.driver_message.data.withdraw = res.data.data
								}
							}.bind(this));
					}.bind(this));


			},

			/**
			 * Переотправка сообщения
			 * Если оно не дошло, при событии переотправки
			 */
			resendMessage: function () {
				app.$event
					.$emit(app.$eventsName.resend, {
						driver_selected: this.driver_selected,
						driver_message: this.driver_message
					});
			},

			/**
			 * Открытие окна верификации
			 * Отсылаем сообзение в шину событий, для открытия попапа
			 */
			openVerificationMessage: function (vId) {
				let
					d = this.driver_message.data,
					_id = !vId.target ? vId : null,
					// дентификатор для подверждения
					id = _id || d && d.verification && d.verification.id,
					// выделеный водила
					driver_selected = id && this.driver_selected,
					//данные для отслыки переотрвки
					data = driver_selected && {
						verification_simple_id: id,
						// выделенный водитель
						driver: this.driver_selected,
						message: this.driver_message
					};
				// зовем показ верефикации + отображение
				data && app.$event.$emit(app.$eventsName.verificationOpen, data);
			},

		},
		computed: {
			expires_in: function () {
				if (this.driver_message.data) {
					return this.driver_message.data.verification && this.driver_message.data.verification.expires_in;
				}
			},

			v_status: function () {
				if (this.driver_message.data && this.driver_message.data.verification) {
					if (this.driver_message.data.verification.status == 'pending' && !this.driver_message.data.verification.expires_in) {
						return 'pause'
					}
					return this.driver_message.data.verification.status;
				}
			},

			// можно ли открытиывать сообщение с таймером
			isExpireVerification: function (params) {
				return (new Date().getTime() - new Date(this.driver_message.data.verification.expires_in * 1000).getTime() < 0)
			},

			// классы доставки сообщений
			classesStatus: function () {
				let
					classesMsg = '',
					driver_message = this.driver_message,
					// отправка
					isSending = driver_message.status == 'sending',
					isDelivered = driver_message.status == 'delivered' || driver_message.status == 'read',
					isUnRead = driver_message.status !== 'read',
					isFiled = driver_message.status == 'fail',

					// классы
					classSending = isSending ? 'sending ' : '',
					classRead = isUnRead ? 'unread ' : '',
					classFail = isFiled ? 'failed' : '',
					classUser = driver_message.sent_by_manager ? 'me' : 'their',
					classType = driver_message.type == 'message' ? 'text' : driver_message.type == 'photo' ? 'photo' : '';

				return driver_message && [
					classSending,
					classRead,
					classUser,
					classType,
					classFail
				].join(' ') || '';
			}
		},

		// чищу таймеры, если они есть
		beforeDestroy() {
			console.log('de')
			clearInterval(this.timeRequest);
			this.timeRequest = null;
		}
	});


})();
