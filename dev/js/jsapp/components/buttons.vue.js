(function (params) {

    /**
     * Кнопки, и общая их обработка
     * -----
     * ** vue-button - обычная кнопка
     * ** vue-button-add - кнопка для добавления с +
     * ** vue-button-load - кнопка для загрузки
     * ** vue-button-video - кнопка для видео - плеера
     */

    Vue.component('vue-button', {
        props: ['caption', 'gradient', 'disabled'],
        data: function () {
            return {
                isGragient: !!this.gradient
            }
        },
        template: '#template-button'
    });

    Vue.component('vue-button-add', {
        props: ['text'],
        template: '#template-button-add'
    });

    Vue.component('vue-button-video', {
        props: ['text'],
        template: '#template-button-video'
    });

    Vue.component('vue-button-load', {
        props: ['text'],
        template: '#template-button-load'
    });

})();