(function (params) {

    /**
     * Слайдер селект
     * @param {*} state 
     */
    Vue.component('vue-slider', {
        template: '#template-slider',
        props: ['name', 'f', 't'],

        /**
         * Предустановленные данны
         * ---
         * ** isActive - флаг о том, открыт ли селект или зактрыт
         * ** from - чило от и до
         * ** to - чило от и до
         */
        data() {
            return {
                isActive: false,
                from: this.f || 1930,
                to: this.t || new Date().getFullYear()
            }
        },

        created() {
            app.$event
                // событие для сброса селекта
                .$on(app.$eventsName.onGlobalClose, section => {
                    this.isActive = false;
                });
        },

        /**
         * Функции, которые
         * будем использовать.
         */
        methods: {
            activated: function () {
                // Когда модель будет изменена, 
                // представление обновится.
                this.isActive = !this.isActive;
            }
        },

        /**
         * Dom Ready
         * @param {*} state 
         */
        mounted: function () {
            var
                self = this,
                $el = $(this.$el),
                slider = $el.find('.slider');
            // ноуслайдер - класс
            noUiSlider
                .create(slider[0], {
                    start: [parseInt(this.from, 10), parseInt(this.to, 10)],
                    connect: true,
                    range: {
                        'min': parseInt(this.from, 10),
                        'max': parseInt(this.to, 10)
                    },
                    step: 1
                });

            let fnEmmit = () => {
                app.$event.$emit(app.$eventsName.onSliderInput, { 'name': this.name, values: [this.to, this.from] });
            }
            let $debounceEmmit = $.debounce(fnEmmit, 250);

            // вешаем события для слайдреа
            // TODO разьебать это как то по другому
            slider[0]
                .noUiSlider
                .on('update', function (values, handle, unencoded, isTap, positions) {
                    self.to = parseInt(values[0]);
                    self.from = parseInt(values[1]);
                    $el.find(".slider-select-value-from").text(self.to);
                    $el.find(".slider-select-value-to").text(self.from);
                    //глбоально событие семены
                    $debounceEmmit.call(self);
                });
        }
    });

})();