(function (params) {

    /**
     * Слайдер селект
     * @param {*} state 
     */
    Vue.component('vue-slider-line', {
        template: '<div style="width: 250px"></div>',

        /**
         * Предустановленные данны
         * ---
         * ** isActive - флаг о том, открыт ли селект или зактрыт
         * ** from - чило от и до
         * ** to - чило от и до
         */
        data() {
            return {
                from: 0,
                to: 10,
                step: 0.1
            }
        },


        /**
         * Dom Ready
         * @param {*} state 
         */
        mounted: function () {
            var
                self = this,
                slider = this.$el,
                $el = $(this.$el);

            // ноуслайдер - класс
            noUiSlider
                .create(slider, {
                    start: this.from,
                    handles: 1,
                    connect: "lower",
                    range: {
                        'min': this.from,
                        'max': this.to
                    },
                    step: .1
                });

            // вешаем события для слайдреа
            // TODO разьебать это как то по другому
            slider
                .noUiSlider
                .on('update', function (values, handle, unencoded, isTap, positions) {
                    self.to = parseFloat(values[0]);
                    self.from = parseFloat(values[1]);
                    $('#companies-actions__label-1').text(self.to);
                    // $('#companies-actions__label-2').text(self.from);
                });
        }
    });


})();