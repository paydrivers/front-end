(function (params) {

    /**
     * Комопнент вью - календарь
     */
    Vue.component('vue-calendar', {
        props: ['text', 'type'],
        template: '#template-calendar'
    });


    window.onload = function () {
        $('.datepicker-inline').datepicker({
            format: "dd-mm-yyyy",
            language: "ru"
        });


    }




})();