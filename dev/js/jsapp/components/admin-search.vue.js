(function (params) {


    // дебонсю изменение фильтра
    $fnEmmitFilter = $.debounce($emmitFilter, 250);

    Vue.component('vue-admin-search', {
        props: ['drivers_count', 'drivers_type_active'],

        mixins: [vueMixins.watchFilterMixin],
        template: '#template-admin-search',
        data: function () {
            return {
                // вывели детей к фльтрам
                search_title: 'Выбрать водителей',
                search_title_text: 'Всего в выборке',

                /**
                 * Массив для марок
                 * И выделенные марок
                 */
                filterMarkOptions: [
                ],
                auto_marks: [],

                /**
                 * Массив для моделей
                 * И выделенные модели
                 */
                filterModelOptions: [
                ],
                auto_models: [],
                filterModelDisabled: true,

                /**
                 * Массив для кузова
                 * И выделенные кузова
                 */
                filterBodyOptions: [
                ],
                auto_series: [],
                filterBodyDisabled: true,

                /**
                 * Массив для цвета
                 * И выделенный элемент цвета
                 */
                filterColorDictionary: {},
                filterColorOptions: [
                ],
                auto_colors: [0],


                auto_year: [],
                driver_age: [],

                // Строка поиска
                q: '',

                // открыт или закрыт поиск
                isOpen: false,

                // коичество выбранных на странице пользователей
                filteredUserCount: 0,
                // выделеные строки
                selectedIds: [],

                // колическо выделенных
                selectedCount: 0,

                // тут будут фильтры дял передачи по событию - дальше
                // ловлю событие в таблице
                filter: {
                }
            }
        },

        // перед началом - запрашиваю марки и цвета
        beforeCreate() {
            getSearchMark.call(this);
            getSearchColor.call(this);
        },

        methods: {

            // установка фильтра
            setFilter: setFilter
        },


    });

    function setCountDriver(m) {
        // this.filteredUserCount = 
    }


    /**
     * Установка фильтров в панель.
     * Дергаем событие on-set-filter
     * @param filterName - название фильтра
     */
    function setFilter(filterName) {

        // срасываю фильтр
        this.filter = [];

        // записываю запии в фильтр для передачи
        // если цвет - то сторю по словарю
        [
            'auto_marks',
            'auto_models',
            'auto_series',
            'auto_colors',
            'auto_year',
            'driver_age',
            'q'
        ]
            .forEach((value) => {
                if (this[value] && !!this[value][0]) {
                    let color = false;
                    if (value == 'auto_colors') {
                        color = this[value].map((v) => {
                            return this.filterColorDictionary[v]
                        });
                    }
                    this.filter[value] = color ? color : this[value];
                }
            });


        // подгружаю селекты - зависимости для фильтров
        switch (filterName) {
            // выбираю марку -> получа модели
            case 'auto_marks':
                getSearchModel.call(this, this[filterName]);
                break;
            // выбираю модель -> 
            case 'auto_models':
                getSearchBody.call(this, this[filterName]);
                break;

        }

        //даю понять что фильтры установлены, через функицб с дебоунс
        $fnEmmitFilter({
            name: filterName,
            filter: this.filter
        });

        // сбрасываем чекбоксы
        app.$event.$emit('on-table-checkbox-reset', true);

    }
    /**
     * Дебонс для устанвоки фильтра
     * @param {*} f  - функиця
     */
    function $emmitFilter(f) {
        app.$event.$emit('on-set-filter', f);
    }

    /**
    * Получаю список кузовов по модели авто
    * Если нет переданного - то запрос не делаю, 
    * очищаю зависимости
    * @param autoId - идентификатр для запроса
    */
    function getSearchBody(autoIds) {
        this.filterBodyOptions = [];
        this.filterBodyDisabled = true;

        // если сбросили - то чищу зависимоти
        if (!(autoIds && autoIds[0])) {
            this.filterBodyOptions = [];
            this.auto_series = [];
            return false;
        }

        let str = '';
        autoIds.forEach(function (itm) {
            str += '&models[]=' + itm
        });
        app
            .HTTP({
                method: 'get',
                url: 'autos/parameters/series/list?' + str
            })
            .then(function (response) {
                response.data.data
                    .forEach(function (option, index) {
                        this.filterBodyOptions.push({
                            'value': option.id,
                            'text': option.name
                        })
                    }.bind(this));

                this.filterBodyDisabled = false;


            }.bind(this))
            .catch(getSearchError.bind(this));

    }

    /**
    * Получаю список моделей авто
    * Если нет переданного - то запрос не делаю, 
    * очищаю зависимости
    * @param autoId - идентификатр для запроса
    */
    function getSearchModel(autoIds) {
        this.filterModelOptions = [];
        this.filterModelDisabled = true


        // если сбросили - то очищаем некст
        if (!(autoIds && autoIds[0])) {
            this.filterModelOptions = [];
            this.filterBodyOptions = [];
            this.auto_models = [];
            this.auto_series = [];
            return false;
        }

        let str = '';
        autoIds.forEach(function (itm) {
            str += '&marks[]=' + itm
        });

        // marks
        app
            .HTTP({
                method: 'get',
                url: 'autos/parameters/models/list?' + str,
            })
            .then(function (response) {
                response.data.data
                    .forEach(function (option, index) {
                        this.filterModelOptions.push({
                            'value': option.id,
                            'text': option.name
                        })
                    }.bind(this));
                this.filterModelDisabled = false;

            }.bind(this))
            .catch(getSearchError.bind(this));

    }


    /**
    * Получаю список марок авто
    */
    function getSearchMark() {
        this.filterMarkOption = [];
        app
            .HTTP({
                method: 'get',
                url: 'autos/parameters/marks/list'
            })
            .then(function (response) {
                response.data.data
                    .forEach(function (option, index) {
                        this.filterMarkOptions.push({
                            'value': option.id,
                            'text': option.name
                        })
                    }.bind(this));
            }.bind(this))
            .catch(getSearchError.bind(this));

    }


    /**
     * Получаю список цветов
     */
    function getSearchColor() {
        app
            .HTTP({
                method: 'get',
                url: 'autos/parameters/colors/list'
            })
            .then(function (response) {
                response.data.data
                    .forEach(function (option, index) {
                        var newId = index + 2;
                        // составляю словарь 
                        // для идентификаторов цвета, т.к.они не инт
                        this.filterColorDictionary[newId] = option.id;
                        this.filterColorOptions.push({
                            'id': newId,
                            'text': option.name,
                            'color': option.code,
                        })
                    }.bind(this));
            }.bind(this))
            .catch(getSearchError.bind(this));
    }



    function getSearchError(error) {
        console.info('--- log : ');
        console.warn(error);
    }
})();