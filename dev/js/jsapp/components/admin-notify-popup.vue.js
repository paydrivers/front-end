(function () {


	// комопнент попавпа для нотификации
	Vue.component('vue-admin-notify-popup', {
		template: '#template-admin-notify-popup',
		props: [
			'unchecked_notify',
		],
		methods: {
			goHistory: function () {
				window.location.href = "/admin/history";
			}
		}

	});

	// комопнент айтема истории и попапа нотификаци
	Vue.component('vue-admin-notify-popup-item', {
		template: '#template-admin-notify-popup-item',
		props: [
			'item_history'
		],
		computed: {
			history_date: function () {
				return this.history_data && this.history_data.created_at ? this.getFormatDateTime(this.history_data.created_at) : this.getFormatDateTime(this.item_history.created_at)
			},
			history_title: function () {
				let text = '';

				switch (this.item_history.type) {
					case 'user_blocking':
						text = 'Пользователь исключен'
						break;
					case 'verification_photo':
						text = 'Фото на проверку'
						break;
				}
				if (this.item_history.type == 'user_blocking' && !this.item_history.user.blocked) {
					text = 'Пользователь разбанен'

				}
				return text;
			},
			history_driver: function () {
				return this.history_data.auto ? this.history_data : this.item_history;
			},
			history_data: function () {
				return this.item_history.polymorph_id ? this.item_history : this.item_history.data;
			}
		},
		methods: {
			// формат таблицы - дата (для каждой строки)
			getFormatDateTime: function (unixtime) {
				return app.getFormatDateTime(new Date(unixtime * 1000));
			},

			/**
			 * Открытие  окна верификации
			 */
			verificationOpen: function () {
				let item_history = this.history_data;

				// говорю - открыть окно верификации, прокидываю данные
				app.$event.$emit(app.$eventsName.verificationOpen, {
					// одиночный просмотр верификацйий
					verification_simple_id: item_history.data.verification.id,
					driver: {
						verification: item_history.data.verification,
						driver: item_history.driver,
						auto: item_history.auto,
						user: item_history.user
					},
					message: this.item_history
				});
			},
		}

	});




})();
