(function () {
	const
		//  a фильтры сверху
		advtArray = [
			'advt_campaigns',
			// 'advt_campaign_types',
			// 'advt_campaign_cities'
		],
		//  a фильтры по возрасту
		agesArray = [
			'driver_age',
			'auto_year'
		],
		// филтры по авто
		autosArray = [
			'auto_marks',
			'auto_models',
			'auto_series',
			'auto_colors',
		],
		// типы сортировки
		sortArray = [
			'auto_name',
			'auto_guide',
			'auto_number',
			'driver_full_name',
			'driver_created_at'
		];
	// установка фильтров в переданный контест
	// @param _f - прнятые фильтры
	// @param item - название фильтра
	let _setFilter = function (_f, item) {
		this.filter[item] = _f[item] ? _f[item] : null;
	};

	/**
	 * Комопннет таблицы
	 */
	Vue.component('vue-table', {
		template: '#template-table',

		// @param drivers_type_active - водители, имеющие активынй стстау(не бан)
		// -    1 - заблокированные, 
		// -    0 - нет
		// @param table - даннаые таблицы
		// @param tableType - тип таблицы, 
		// -    blacklist без
		props: ['drivers_type_active', 'table'],
		data: function (params) {
			return {

				// // загрузка
				// loading: false,

				// Поля сортировки
				sort: [
					{
						name: 'auto_name',
						direction: 'asc'
					}, {
						name: 'auto_guide',
						direction: ''
					}, {
						name: 'auto_number',
						direction: ''
					}, {
						name: 'driver_full_name',
						direction: ''
					}, {
						name: 'driver_created_at',
						direction: ''
					}
				],

				// контроль стрелок сортировки
				sortarrows: {
					'auto_name': '',
					'auto_guide': '',
					'auto_number': '',
					'driver_full_name': '',
					'driver_created_at': ''
				},

				//тут будут выделенные id
				selectedIds: [],
				selectedCount: 0,

				// Это айтемы таблицы
				driverList: this.table || [],
				countRecords: 0,

				tableSortArray: [],
				// тут будет строка для фильтрации
				order: '',

				// фильтры тут
				filter: {
					// верх
					advt_campaigns: null,
					advt_campaign_types: null,
					advt_campaign_cities: null,
					// сами фмильтры
					auto_marks: null,
					auto_models: null,
					auto_series: null,
					auto_colors: null,
					driver_age: null,
					auto_year: null,
					q: null
				}
			}
		},



		computed: {
			// тип таблицы
			isBunnedTab: function () {
				return !!this.drivers_type_active;
			},


			//туту сортировки блять, для полей,(счетики)
			indexSort_auto_name: function () {
				return this.getTableSortItem('auto_name');
			},
			indexSort_auto_guide: function () {
				return this.getTableSortItem('auto_guide');
			},
			indexSort_auto_number: function () {
				return this.getTableSortItem('auto_number');
			},
			indexSort_driver_full_name: function () {
				return this.getTableSortItem('driver_full_name');
			},
			index_sort_driver_created_at() {
				return this.getTableSortItem('driver_created_at');
			},
		},

		watch: {
			sort: function (sort) {
				this.setSort();
			}
		},

		mixins: [vueMixins.tableCheckboxMixin, vueMixins.getDriverList],

		created() {

			var self = this;

			// дебонсю меиод установки фильтров
			this.$setFilter = $.debounce(this.setFilter, 0);

			// генерирую сортировку
			this.setSort();

			// сетчу фильтры от смены компаний
			app.$event
				.$on('on-set-company', function (filter) {
					if (filter.filter.advt_campaigns == "999999" && filter.filter.advt_campaign_types == "999999") {

						alert('Создаем гонку или переходим на существующую, если она есть');

					} else {
						advtArray.forEach(_setFilter.bind(this, filter.filter));
						this.setSort();
					}
				}.bind(this));

			// сетчу фильты от фильтров, для запроса к таблице
			app.$event
				.$on('on-set-filter', function (filter) {
					agesArray.forEach(_setFilter.bind(this, filter.filter));
					autosArray.forEach(_setFilter.bind(this, filter.filter));
					_setFilter.call(this, filter.filter, 'q');
					this.setSort();
				}.bind(this));

			//сброс чекбоксов
			app.$event
				.$on('on-table-checkbox-reset', function (data) {
					this.selectedIds = [];
					this.selectedCount = 0;
				}.bind(this));


			// собновление таблицы
			app.$event
				.$on(app.$eventsName.onUpdate, function (eData) {

					// блокеротор
					if (eData.type == "blocked") {

						// заменяю драйвер лист новым
						this.driverList = this.driverList.filter(function (driver_item, index) {
							return !(eData.selectedKeys.indexOf(driver_item.user.id) + 1);
						});

						// снимаю чекбоксы
						app.$event.$emit('on-table-checkbox-reset', true);
					}

					// меню строки на изменение коэффицциента
					if (eData.type == "coeff") {
						this.driverList
							.forEach(function (driver_item, key) {
								if (eData.selectedKeys.indexOf(driver_item.user.id) + 1) {
									var dl = this.driverList[key];
									dl.driver.rate = parseFloat(eData.value);
									this.$set(this.driverList, key, dl);
								}
							}.bind(this));
					}

				}.bind(this))
		},

		methods: {

			/**
			 * Гоу на чет водителлей.
			 */
			goChatDriver: function (id) {
				localStorage.setItem('chat_selected_item', id);
				window.location.href = "/admin/chat";
			},


			getTableSortItem: function name(name) {
				let
					isCompare = false,
					columnName = name,
					tableItem = this.tableSortArray.filter(function (item) {
						return (item.name === columnName);
					})[0];

				return tableItem ? tableItem.position : '';
			},

			// формат таблицы -  возраст
			declOfNumYear: function (age) {
				return age + ' ' + app.declOfNum(age, [' год', ' года', ' лет']);
			},


			oneBanned: function (id) {
				app.$event
					.$emit('on-user-banned', id);
			},

			setSort: function (name, direction) {
				var srt = '';
				this.tableSortArray.forEach(function (item) {
					if (item.direction) {
						srt += '&orders[' + item.name + '][direction]=' + item.direction + '&orders[' + item.name + '][position]=' + item.position;
					}
				});
				this.order = srt;
				this.$setFilter();
			},
			setFilter: function () {

				let
					filters = 'drivers/list?',
					// кеширую фильтры
					f = this.filter,
					is_bloceked = this.drivers_type_active ? 1 : 0,
					// смотрю только на запятые
					q = f.q && f.q.split(','),
					_srt_f = '&filters[',
					_str_r = '][]=',
					_srt_l = ']=';

				// ограничиваю запросы еслм фильтров нет 
				if (this.drivers_type_active == 0 && (!this.filter.driver_age || !this.filter.auto_year)) {
					return false;
				}

				// задаю пагинацию и сортировку
				filters += 'pagination[page]=1&pagination[count_per_page]=10000';
				filters += this.order;

				//Фильтр: заблокированные пользователи (1 - заблокированные, 0 - нет)
				filters += '&filters[users_blocked][state]=' + is_bloceked;

				// текстовый поиск
				q && q.forEach(function (item) {
					filters += (_srt_f + 'q' + _str_r + item);
				});

				// верха, реклама
				advtArray
					.forEach(function (item) {
						f[item] && (filters += (_srt_f + item + _str_r + f[item]));
					});

				// даты от да
				agesArray
					.forEach(function (item) {
						f[item] && (filters += (_srt_f + item + '][from' + _srt_l + f[item][0] + _srt_f + item + '][to' + _srt_l + f[item][1]));
					});

				// автомобили
				autosArray
					.forEach(function (item) {
						f[item] && f[item].forEach(function (itm) {
							(filters += (_srt_f + item + _str_r + itm));
						});
					});

				// this.loading = true;
				app.$event.$emit(app.$eventsName.onLoad, false);

				// получаю список водил
				this.getDriverList(filters);

			}
		}
	});


})();
