(function (params) {

    /**
     * Комопнент вью - чекбокс
     */
    Vue.component('vue-checkbox', {
        template: '#template-checkbox',


        props: ['checked'],

        /**
         * Для инпута - нужна модель.
         * Свойство - чекед, данные пристваиваем те которые передали
         */
        model: {
            prop: 'checked'
        },

        /**
         * Предустановленные данны
         * ---
         * ** isChecked - флаг о том, открыт ли селект или зактрыт
         */
        data: function () {
            return {
                isChecked: this.checked
            }
        },

        /**
         * Dom Ready
         * @param {*} state 
         */
        mounted: function () {
            console.log(this.isChecked)
        },

        /**
         * Вотчет свойств из data(){}
         */
        watch: {
            isChecked: function (val, oldVal) {
                console.info('new: %s,\nold: %s', val, oldVal)
            }
        }
    });



})();