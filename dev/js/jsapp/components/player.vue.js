(function (params) {

    Vue.component('vue-player', {
        props: ['text'],
        template: '#template-player'
    });

})();