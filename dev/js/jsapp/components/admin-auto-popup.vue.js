(function () {
	// комопнент верификации
	Vue.component('vue-admin-verification-popup', {
		template: '#template-admin-verification-popup',

		props: [
			// данные о верификации, 
			// которые прокинуты сверху
			// .verification_simple_id - идентификатор для верификации
			// .driver - информация о водителе и авто
			// .message - собщение по которому клиекнули
			'verification'
		],

		data: function () {
			return {
				loading: true,
				// данные о верефикации, 
				// получаю с бл .getData
				verification_full: null,
				// текст для перезапроса фтографии
				textToDriver: '',
				// показывать ли текст для водителя
				isShowTextToDriver: false,
				// сообение которые было открыто
				message: this.verification.message,
			}
		},
		created() {
			// Получаю днные, если есть идентификатор верификации
			this.verification &&
				this.verification.verification_simple_id &&
				this.getData(this.verification.verification_simple_id);
		},
		mounted() {

			/// закишироваьли имг
			this.$images = $(this.$el).find('.auto-popup-item__image');

			// Закрыьтваю
			app.$event.$on(app.$eventsName.onGlobalClose, () => this.showToggle(false));
		},

		computed: {
			'isBlocked': function () {
				return this.verification && this.verification.driver && this.verification.driver.user.blocked
			},
			// 	вВкр запроса
			'hasButtonSuccess': function () {
				return this.verification_full && this.verification_full.status === 'success';;
			},
			// 	вВкр запроса
			'hasFailed': function () {
				return this.verification_full && this.verification_full.status === 'failed';;
			},
			// нужна ли кнопка аппрува?
			'hasButtonApprove': function () {
				let
					// верификацияонные картинки
					data_photo =
						this.verification_full &&
						this.verification_full.photos,

					// если ли аппрувы
					data_photo_count = data_photo && data_photo.length,

					data_photo_last_status_rejected = data_photo_count && data_photo.filter(function (item) {
						return item.status == 'rejected';
					}),
					data_photo_last_status_pending = data_photo_count && data_photo.filter(function (item) {
						return item.status == 'pending';
					}),
					data_photo_last_status_approved = data_photo_count && data_photo.filter(function (item) {
						return item.status == 'approved';
					}),
					hasButton = true,

					isReject = data_photo_last_status_rejected && data_photo_last_status_rejected.length,
					isApproved = data_photo_last_status_approved && data_photo_last_status_approved.length,
					isPending = data_photo_last_status_pending && data_photo_last_status_pending.length;

				if (isReject && isPending) {
					hasButton = 0;
				} else if (isReject) {
					hasButton = 2
				} else {
					hasButton = 1;
				}

				return hasButton;
			}
		},


		methods: {
			// формат таблицы - дата (для каждой строки)
			getFormatDateTime: function (unixtime) {
				return app.getFormatDateTime(new Date(unixtime * 1000));
			},
			goHistory: function () {
				window.location.href = "/admin/history";
			},
			// показываеть или скрывает. Шоет сбытие на верх, для сокрытия из вне
			showToggle: function (isShow) {
				app.$event.$emit(app.$eventsName.verificationOpen, (isShow && this.verification) || null);
			},

			/**
			 * Бан водителя
			 */
			bunDriver: function () {
				// сначала - делаем  реджект верификации
				this.verificationReject(() => {
					// баннним
					app.bunController({
						state: true,
						users_ids: [this.verification.driver.user.id],
						reason: 'Все попытки верификации - провалены'
					})
						.then(function (response) {
							this.showToggle(false);

							this.verification_full.status = 'failed';

							// установка блокировкаи
							this.verification.driver.user.blocked = true;

							// редактивраоние сообщений
							updaterMessages.call(this, 'failed', this.verification_full);

							// шлю нотификаию, для обработки из вне, в чате и истори и нотификации - запросов
							app.$event.$emit('on-verification-status-change', this.verification_full);


						}.bind(this))
				});

			},

			showFieldRequest: function () {
				this.isShowTextToDriver = true;
			},

			/**
			 * Отклониение фото-врифкации, запрос доп фото
			 * 1. Отклоняем фотографию верификацию
			 * 2. Если фотографии уже 2(новые)+1(была) - ?
			 * 3. Если фото еще можно запросить - запрашиваю
			 */
			verificationReject: function (fn) {
				// реджект фотографии
				photoVerification.call(this, 'rejected', (data) => {
					let
						verification = this.verification_full,
						// беру фотк 
						verificationPhoto = verification.photos,
						// количетсво запросов
						verificationPhotoCount = verificationPhoto.length,
						// беру полсдею верривификаю
						verificationLastPhoto = verificationPhoto &&
							verificationPhotoCount &&
							verificationPhoto[verificationPhotoCount - 1];

					// если фотка всего одна
					// то запрашиваю еще одну, и реджекчу 
					if (verificationPhotoCount < 2) {
						// Если есть варик еще запросить - то запрашиваю
						verificationResend.call(this, () => {
							// ставлю фотке режект
							this.verification_full.photos.forEach(item => {
								if (item.status !== 'rejected') {
									item.status = 'rejected';
								}
							});
							app.$event.$emit('on-message-verification-status-change', true);

							// шлю нотификаию, для обработки из вне, в чате и истори и нотификации - запросов
							app.$event.$emit('on-verification-status-change', this.verification_full);
						});

					} else {
						// Отправка запроса на заваленную верификацию
						verificationComplete.call(this, 'failed', () => {// ставлю фотке режект
							// ставлю фотке режект
							this.verification_full.photos.forEach(item => {
								if (item.status !== 'rejected') {
									item.status = 'rejected';
								}
							});
							// вызываю баннед
							fn && fn();
						});
					}

				})
			},

			/**
			 * аппрув фотографии
			 * Если я аппрувлю фото - то значит просто выхожу из окна аппрува
			 */
			verificationApprove: function name() {
				photoVerification.call(this, 'approved', (data) => {
					this.verification_full.status = 'success';
					// шлю нотификаию, для обработки из вне, в чате и истори и нотификации - запросов
					app.$event.$emit('on-verification-status-change', this.verification_full);
					this.verification_full = false;
					this.showToggle(false);
					updaterMessages.call(this, 'success', this.verification_full);

					app.$event.$emit('on-message-verification-status-change', false);

				});

			},

			// получаю данные по верификации
			getData: function () {
				let
					// хедер для авторизщации
					headersAuth = { 'Authorization': app.getToken() },
					// хшр
					xhr = app.HTTP,
					// опции для запроса
					xhrOptions = {
						method: 'get',
						url: ['verification/', this.verification.verification_simple_id].join(''),
						headers: headersAuth
					};
				xhr(xhrOptions)
					.then((data) => {
						this.verification_full = data.data.data
						$('.auto-popup-item__image')
							.imagesLoaded()
							.done(() => {
								this.loading = false;
							});
					})
					.catch(function (params) {
						this.verification_full = null
						this.showToggle(false);
						this.loading = false;
						alert('Ошибка чтения верификации!');
					}.bind(this));

			}
		}
	});


	/**
	 * Метод для обновления сообщений верификации
	 * @param status 
	 */
	function updaterMessages(status, msg) {
		if (msg && msg.data && msg.verification) {
			msg.data.verification.status = status;
			msg.data = Object.assign({}, msg.data);
		}
	}


	function photoVerification(type, fn) {
		let
			verification = this.verification_full,
			// беру фотк 
			verificationPhoto = verification.photos,
			// количетсво запросов
			verificationPhotoCount = verificationPhoto && verificationPhoto.length,
			// беру полсдею верривификаю
			verificationLastPhoto = verificationPhotoCount && verificationPhoto.filter(function (item) {
				return item.status == 'pending';
			}),
			// url
			url = verificationLastPhoto && verificationLastPhoto[0] && [
				'verification/',
				verification.id,
				'/photo/',
				verificationLastPhoto[0].id,
				'/',
				type
			].join(''),
			// хедер для авторизщации
			headersAuth = { 'Authorization': app.getToken() },
			// хшр
			xhr = app.HTTP,
			// опции для запроса
			xhrOptions = {
				method: 'post',
				url: url,
				headers: headersAuth
			};

		fn.call(this);

		url && xhr(xhrOptions)
			.then(function () {
				this.showToggle(false);
			}.bind(this))

	}

	/**
	 * Окончание верификации
	 * @param {*} type  - реджект, аппрув
	 * @param {*} fn  - метод для вырпления
	 */
	function verificationComplete(type, fn) {
		let
			verification = this.verification_full,
			// беру фотк 
			verificationPhoto = verification.photos,
			// количетсво запросов
			verificationPhotoCount = verificationPhoto && verificationPhoto.length,
			// беру полсдею верривификаю
			verificationLastPhoto = verificationPhotoCount && verificationPhoto[verificationPhotoCount - 1],
			// url_sendMessageText
			url = verificationLastPhoto && [
				'verification/',
				verification.id,
				'/',
				type
			].join(''),
			// хедер для авторизщации
			headersAuth = { 'Authorization': app.getToken() },
			// хшр
			xhr = app.HTTP,
			// опции для запроса
			xhrOptions = {
				method: 'post',
				url: url,
				headers: headersAuth
			};

		fn.call(this);

		url && xhr(xhrOptions)
			.then(function () {
				this.showToggle(false);
				fn && fn();
			}.bind(this))

	}


	/**
	 * Отправка заказного запроса фото
	 */
	function verificationResend(fn) {
		this.loading = true;

		let
			// уид для сообщени
			uuid = app.getHash(),
			// идентификатор водилы
			driverId = this.verification.driver.user.id,
			// данны для отправки
			sendData = {
				// text: (this.textToDriver || app.defaultReVerificationText),
			},
			// данныке для отправки
			messageData = {
				"uuid": uuid,
				"message": sendData.text
			},
			// хедер для авторизщации
			headersAuth = { 'Authorization': app.getToken() },
			// хшр
			xhr = app.HTTP,
			// опции для запроса
			xhrOptions = {
				method: 'post',
				url: ['chat/messages/simple/send/', driverId].join(''),
				headers: headersAuth,
				data: messageData
			};

		// отсылаю сообщение
		xhr(xhrOptions)
			.then((data) => {
				this.loading = false;
				fn && fn();
			})
			.catch(function name() {
				this.loading = false;
			}.bind(this));

	}
})();
