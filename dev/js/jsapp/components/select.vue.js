(function (params) {

    /**
     * Обычный селект
     * @param {*} state 
     */
    Vue.component('vue-select', {
        template: '#template-select',
        // options - опшены
        // value - выбранный
        // если у нас передан url - то это автокомплит
        // placeholder - пйлейсхолде
        props: ['options', 'value', 'url', 'placeholder', 'disabled', 'ajaxload'],
        data: function () {
            var self = this;
            return {
                // dis: this.disabled,
                ajaxOptions: {
                    url: app.apiuri + this.url,
                    dataType: 'json',
                    delay: 390,
                    tags: false,

                    /**
                     * Полученый рузельтат разбиваем 
                     * на нужный нам формат
                     */
                    processResults: function (data, params) {
                        var arr = data.data.map(function (item) {
                                return {
                                    id: item.id || item.advt_campaign_id,
                                    text: item.name || item.advt_campaign_name
                                }
                            });
                            // arr.push({id: '-1', text: 'Не выбрано' })
                        return {
                            results: arr
                        }
                    },

                    /**
                     * Кастомный писк для селекта,
                     *  через jquery
                     */
                    transport: function (params, success, failure) {
                        let ajax = $.ajax({
                            method: 'get',
                            url: this.url,
                            data: {
                                q: params.data.q
                            },
                            headers: {
                                'Authorization': app.getToken()
                            }
                        }).then(success).catch(failure);
                        return ajax;
                    }
                }
            }
        },
        destroyed: function () {
            $(this.$el)
                .find('select')
                .off()
                .select2('destroy');
        },

        /**
         * Dom Ready
         * Монтируем селект2
         * @param {*} state 
         */
        mounted: function () {
            let self = this;

            let $select2 = $(this.$el).find('select');

            // если у нас передан урл - то это автокомплит
            if (this.url) {
                $select2
                    .select2({
                        language: 'ru',
                        allowClear: true,
                        placeholder: this.placeholder,
                        ajax: this.ajaxOptions,
                        // dropdownCssClass: 'filter-search'
                    });
            } else {
                $select2
                    .select2({
                        language: 'ru',
                        allowClear: this.allowclose,
                        placeholder: this.placeholder
                    });
            }

            $select2
                .val(this.value)
                .on('change', function () {
                    self.$emit('input', this.value);
                })
                .trigger('change')


        },
        watch: {
            value: function (value) {
                // if (this.url) {
                // update value
                $(this.$el)
                    .find('select')
                    .val(value)
                    .trigger('change');
                // }

            },
            options: function (options) {
                // if (this.url) {
                // update options
                $(this.$el)
                    .find('select')
                    .empty()
                    .select2({
                        data: options
                    });
                // }
            },

        },
        methods: {

        }
    });


})();