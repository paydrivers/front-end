(function () {

	/**
	 * .verificationOpen - Событие открытие откна верификации.
	 * Открывается из `vue-context`
	 * @event 'on-toggle-verification-popup'
	 * @param verification_simple_id - одиночная верификация
	 * @param driver - выделенный айтема
	 * 
	 * .resend - Пересылка соощений
	 * @event 'on-resend-message'
	 * @param driver_selected - выделенный водитель
	 * @param driver_message - сообщение водителея
	 *
	 * .onSliderInput сладейр - измененение
	 * @event 'on-slider-change',
	 *
	 * .onLoad загрузка закончена
	 * @event 'on-table-load',
	 *
	 * .onUpdate загрузка закончена
	 * @event 'on-table-row-update',
	 * 
	 * .onGlobalClose массовый обработчки закртыия яи эск
	 * @event 'on-global-close',
	 * 
	 * .onAddChatMessage сообщние в чат
	 * @event 'on-chat-new-message',
	 * 
	 * .onNewMessage сообщение в шину - сообщение
	 * @event 'on-message-new-message',
	 *
	 * .onNewNotify сообщение в шину - нотификаци
	 * @event 'on-message-new-notify'
	 *
	 * .onMessageCounterUpdate сообщение в шину - нотификаци
	 * @event 'on-messages-counters'
	 */
	const $eventsName = {
		verificationOpen: 'on-toggle-verification-popup',
		onSliderInput: 'on-slider-change',
		resend: 'on-resend-message',
		onLoad: 'on-table-load',
		onUpdate: 'on-table-row-update',
		onGlobalClose: 'on-global-close',
		onAddChatMessage: 'on-chat-new-message',
		onNewMessage: 'on-message-new-message',
		onMessageCounterUpdate: 'on-messages-counters',
	};


	// типы роутов авторизации
	const AuthTypes = ['login', 'recovery', 'message'];
	const Routes = ['dashboard', 'news', 'history', 'chat', 'settings', 'blacklist', 'race', 'points'];
	const infinityScrollTargetHeight = 0;
	const uri = 'https://www.api.paydrivers.com/v1';
	const checkconnecturi = 'https://www.api.paydrivers.com/v1/settings/list/socials';
	const headers = {
		'Content-Type': 'application/json'
	};
	const _axiosBase = axios.create({
		baseURL: uri,
		responseType: 'json',
		headers: headers
	});
	const defaultVerificationText = 'Здравствуйте. Мы проводим плановую проверку расклейки рекламных кампаний.\nВ течение времени ниже - пришлите, пожалуйста, фото вашего автомобиля.';
	const defaultReVerificationText = 'Здравствуйте. Мы смогли сопоставить фотографию вашего автомобиля с первоначаной фотографией после обклейки. \nВ течение времени ниже - пришлите, пожалуйста, новое фото вашего автомобиля, еще раз.';

	window.app = {

		// виды что объявлены
		View: {},

		// модели что объявлены
		Model: {},

		// вид, которуй уже создан
		views: {},

		// модель, которая уже создана
		models: {},

		// миксины вью
		vueMixins: {},

		// шина событий
		$event: new Vue({}),

		// урл для апи
		apiuri: uri,
		// заголовки по ддефу
		apiheaders: headers,

		RoutesConst: Routes,
		AuthTypes: AuthTypes,

		// высота-остаток скролла, при 
		// котором начиенается загрузка данных
		infinityScrollTargetHeight: infinityScrollTargetHeight,


		// дефаут текстф для стращных
		defaultVerificationText: defaultVerificationText,
		defaultReVerificationText: defaultReVerificationText,

		// всплытие для словеьт
		// пытаюсь загасить попап - астреляется и другие события
		isBubblingDrop: function name(params) {
			return !!$('[bubbling-pupup]:visible').length
		},

		//события глобальные
		$eventsName: $eventsName,

		// опции для проверки подключения на онлайн
		checkOnlineOptions: {
			checks: {
				xhr: {
					url: checkconnecturi
				}
			},
			checkOnLoad: true,
			reconnect: {
				initialDelay: 1,
			},
			requests: true,
			game: false
		},

		// дефолтный коэф для водителей
		defCoeff: 0,

		validator: {
			email: function email(em) {
				return !(!em || em.length < 3 || em.length > 255)
					&& /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,10})$/.test(em);
			},
			social: function name(lnk, url) {
				return lnk.match('http://' + url) || lnk.match('https://' + url)
					|| lnk.match('http://www.' + url) || lnk.match('https://www.' + url)
					|| lnk.match('//www.' + url) || lnk.match('//' + url)
					|| lnk.match('www.' + url) || lnk.match('' + url)

			}
		},


		/**
		 * 
		 * Глобальная шина событий, 
		 * Для разных хуевин
		 * 
		 * ----
		 * 
		 * 
		 */
		$router: {
			// меняем роут с пушстейт
			changeRoute: function (obj) {
				history.pushState({ route: obj.href }, obj.title, obj.href);
				app.$event.$emit('on-push-state', obj);
			},

			getRoute: function () {
				return window.location;
			},


			/**
			 * Получение хрефа, 
			 * для изменеия модели при измении пути
			 */
			getRouteHref: function () {
				return location.pathname
			},


			/**
			* Получаю второй хеш у роута, 
			* для определения типа авторизации
			*/
			getRouteAuth: function () {
				return location.pathname.split('/').splice(1);
			},

			/**
			 * фиксируем определенные этапы авторизации, 
			 * парсим строку на совпадения с оригиналом и подставляем подобный
			 */
			setRouteAuth: function () {
				let secondLevel = this.getRouteAuth()[1];

				return '/auth/' + (AuthTypes.map(function name(value) {
					if (secondLevel && secondLevel.indexOf(value) + 1) {
						return value;
					}
				}).join('') || AuthTypes[0]);
			},

			/**
			 * Получаю сессию, либо кидаю на авторизацию
			 */
			getSession: function () {
				let token = localStorage.getItem('token_auth');
				// если есть токен нет - то кидаю на логин
				if (!token) {
					let _route = this.setRouteAuth();
					app.$router.changeRoute({
						href: _route,
						title: 'Авторизация'
					});
					return false;
				}
				return true;
			}
		},


		// очистаа стора от всего
		clearStor: function () {
			localStorage.setItem('token_auth', '');
			localStorage.setItem('type_page', '');
			// location.reload();
			app.$router.changeRoute({
				href: '/auth/login',
				title: 'Авторизация'
			});
			location.reload();
		},

		//  аксесс токен, для авторизованных запросов
		getToken: function () {
			return localStorage.getItem('token_auth') ? 'Bearer ' + JSON.parse(localStorage.getItem('token_auth')) : '';
		},

		// получею прото объектпрофиля
		getProfile: function () {
			return localStorage.getItem('profile') && JSON.parse(localStorage.getItem('profile'))
		},

		// получаешь профильней
		getProfileName: function () {
			var p = this.getProfile()
			return p && p.profile && p.profile.name
		},

		// склоняет слово по числу. app.declOfNum(23,['мяч', 'мяча', 'мячей']
		declOfNum: function (number, titles) {
			cases = [2, 0, 1, 1, 1, 2];
			return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
		},
		// бьет по разрядам 10000 => 10 000
		bitOfNumber: function (number) {
			return (number.toString && number.toString()).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		},

		// формат даты. сегодня, вчера. Можно переписать, это я юзал на неклототрых проектах
		getFormatDateTime: function (date, isSmall) {
			let d = (new Date());
			date = new Date(date.getTime())
			let
				year = date.getUTCFullYear(),
				month = window.monthsArray[date.getUTCMonth()].toLowerCase().slice(0, 3),
				day = addIntZero(date.getUTCDate()),
				hour = addIntZero(date.getUTCHours()),
				minutes = addIntZero(date.getUTCMinutes()),
				isYc = year == d.getUTCFullYear(),
				isMc = month == window.monthsArray[d.getUTCMonth()].toLowerCase().slice(0, 3),
				isDc = day == addIntZero(d.getUTCDate()),
				isDcc = day == d.getUTCDate() - 1, txtDate = '';
			year = isYc ? '' : year;

			if (isSmall) {
				month = addIntZero(date.getUTCMonth() + 1)
			}

			if (!isSmall) {
				txtDate = day + " " + month + " " + year;
				txtDate = (isYc && isMc && isDc ? 'Сегодня' : (isYc && isMc && isDcc ? 'Вчера' : txtDate))
			} else {
				if (isYc && isMc && isDc) {
					txtDate = '';
				} else if (isYc && isMc && isDcc) {
					txtDate = 'вчера ';
				} else {
					txtDate = day + "." + month + ' ';
				}
			}

			return txtDate + (!isSmall ? ' в ' : '') + hour + ':' + minutes;
		},

		/**
		 * Метод сортировки по дате созвадния,
		 * для водителей чата и сообщений в открытом чате
		 * Отличия  - наличие поля .message в кнорне объекта
		 * @param {*} a 
		 * @param {*} b 
		 */
		sortByCreatedAt: function (a, b) {
			let
				comparer = this,
				opt = ['message', 'created_at'],
				__a = a[opt[1]] || a[opt[0]],
				__b = b[opt[1]] || b[opt[0]];
			// если это драйвер лист(время - в сообщениях)
			if (__a && __b) {
				let
					_a = __a || __a[opt[1]],
					_b = __b || __b[opt[1]];
				return eval(['_a ', '>=', ' _b ? -1 : 1'].join(''));
			}
			return false;
		},

		// объеиненеие
		extend: function (obj, src) {
			Object.keys(src).forEach(function (key) {
				obj[key] = src[key];
			});
			return obj;
		},

		// метод дял создания хеша, для записи модели.
		getHash: function () {
			var uuid = "", i, random;
			for (i = 0; i < 32; i++) {
				random = Math.random() * 16 | 0;
				if (i == 8 || i == 12 || i == 16 || i == 20) {
					uuid += "-"
				}
				uuid += (i == 12 ? 4 : (i == 16 ? (random & 3 | 8) : random)).toString(16);
			}
			return uuid;
		},

		// добавляем ноль
		addIntZero: window.addIntZero,

		// осталось время
		remainingTime: function (endTimestemp) {

			let
				_gTiem = new Date().getTime(),
				_eTime = endTimestemp,
				remaining = endTimestemp ? endTimestemp - new Date().getTime() : 0;

			console.log(remaining)
			// если осталось меньше
			//		секунды - то время ышло и выходи\
			// 
			if (remaining < 1000) {
				clearInterval(this.timeRequest);
				this.timeRequest = null;
			} else {
				let
					oneSec = 1000,
					oneMin = oneSec * 60,
					oneHour = oneMin * 60,
					oneDay = oneHour * 24,
					dday = Math.floor(remaining / oneDay * 1),
					dhour = window.addIntZero(Math.floor((remaining % oneDay) / oneHour * 1)),
					dmin = window.addIntZero(Math.floor(((remaining % oneDay) % oneHour) / oneMin * 1)),
					dsec = window.addIntZero(Math.floor((((remaining % oneDay) % oneHour) % oneMin) / oneSec * 1));

				return dday + "д. " + dhour + "ч " + dmin + "м " + dsec + "с";
			}

		},

		// запростик
		HTTP: _axiosBase,

		// контроллер бана для юзеров
		bunController: function (data) {
			return _axiosBase({
				method: 'post',
				url: 'users/blocking',
				headers: {
					'Authorization': app.getToken()
				},
				data: data
			})
		}
	}

	/**
	 * Поулчает дату в формате дд-мм-гггг
	 *
	 */
	window.monthsArray = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
	/**
	 * Добавляю ноль к инту
	 *
	 */
	window.addIntZero = function (int) {
		return int < 10 ? ("0" + int) : int;
	};


	// document.addEventListener("DOMContentLoaded", function (params) {
	// 	alert();
	// });
})();