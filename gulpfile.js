var
	gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	concat = require('gulp-concat'),
	concatSourcemap = require('gulp-concat-sourcemap'),
	sourcemaps = require('gulp-sourcemaps'),
	csso = require('gulp-csso'),
	pug = require('gulp-pug'),
	livereload = require('gulp-livereload'),
	uglify = require('gulp-uglify'), // минификатор
	webserver = require('gulp-webserver'),
	stylus = require('gulp-stylus'),

	/**
	 * Скрипты 
	 * для конката и вотча
	 */
	scriptsRet = function () {
		return [
			'./dev/js/jsapp/layout/mixins.vue.js',
			// сам вью
			'./dev/libs/vuejs/vue.js',

			//axios - запросы
			'./dev/libs/centrifuge/centrifuge.js',

			'./dev/libs/vuejs/axios.js',

			'./dev/libs/jquery-3/jq.js',
			'./dev/libs/offline/offline.js',

			// инпуты
			'./dev/js/jsapp/components/input.vue.js',
			'./dev/js/jsapp/components/textarea.vue.js',
			'./dev/js/jsapp/components/checkbox.vue.js',
			'./dev/js/jsapp/components/toggle.vue.js',
			'./dev/js/jsapp/components/calendar.vue.js',

			// селекты
			'./dev/js/jsapp/components/select.vue.js',
			'./dev/js/jsapp/components/select-multi.vue.js',
			'./dev/js/jsapp/components/select-multi-color.vue.js',
			'./dev/js/jsapp/components/select-slider.vue.js',
			'./dev/js/jsapp/components/slider.vue.js',


			'./dev/js/jsapp/components/buttons.vue.js',
			'./dev/js/jsapp/components/player.vue.js',

			'./dev/js/jsapp/components/table.vue.js',

			'./dev/js/jsapp/components/admin-tabs.vue.js',
			'./dev/js/jsapp/components/admin-create.vue.js',
			'./dev/js/jsapp/components/admin-search.vue.js',
			'./dev/js/jsapp/components/admin-chat-message.vue.js',
			'./dev/js/jsapp/components/admin-auto-popup.vue.js',
			'./dev/js/jsapp/components/admin-notify-popup.vue.js',
			'./dev/js/jsapp/components/admin-points.vue.js',

			'./dev/js/jsapp/layout/auth.vue.js',
			'./dev/js/jsapp/layout/header.vue.js',
			'./dev/js/jsapp/layout/content.vue.js',

			// 'vue-select'
			'./dev/js/jsapp/pages/admin-points.vue.js',
			'./dev/js/jsapp/pages/admin-blacklist.vue.js',
			'./dev/js/jsapp/pages/admin-settings.vue.js',
			'./dev/js/jsapp/pages/admin-dashboard.vue.js',
			'./dev/js/jsapp/pages/admin-history.vue.js',
			'./dev/js/jsapp/pages/admin-chat.vue.js',
			'./dev/js/jsapp/pages/admin-news.vue.js',
			'./dev/js/jsapp/pages/admin-race.vue.js',


			/**
			 * Либы
			 * ** jquery
			 * ** <Сюда добалять либы js. css инмортить в layout.styl>
			 * ** nouislider слайдео
			 * ** modal - модлка
			 * ** datepicker - дейтпикер
			 * ** select2 - селекты
			 */
			'./dev/libs/noSlider/nouislider.js',

			'./dev/libs/bootstrap-4.1.1/dist/js/modal.js',
			'./dev/libs/bootstrap-4.1.1/dist/js/datepicker.js',
			'./dev/libs/bootstrap-4.1.1/dist/js/datepicker-ru.js',

			'./dev/libs/select2/js/select2.full.js',
			'./dev/libs/select2/js/i18n/ru.js',

			/**
			 * Хелперы
			 * ** можно расширить обзект app/window
			 */
			'./dev/js/jsapp/init-helpers.js',
			'./dev/js/jsapp/admin-actions.js',

			/**
			 * Хелперы
			 * ** Инит риложения
			 */
			'./dev/js/jsapp/init-router.js',

			// сокеты
			'./dev/js/jsapp/init-sockets.js'

		];
	},

	// Пути к файлам
	path = {
		// stylus
		styl: {
			// Тут все импорты
			source: ['./dev/styl/layout/layout.styl'],
			// вотчу стили в либах и стилусе
			watch: ['./dev/styl/*.styl', './dev/styl/**/*.styl'],
			// назначение
			destination: './pub/pages/assets/css/',
			// название файла
			fileName: 'css.css'
		},

		// шаблонизатор pug
		pug: {
			source: ['./dev/pug/**/*.pug'],
			watch: './dev/pug/**/*.*',
			destination: './pub/'
		},

		// svg
		svg: {
			source: ['./dev/svg/**/*.*'],
			watch: './dev/svg/**/*.*',
			destination: './pub/pages/assets/imgs/'
		},
		// svg
		fonts: {
			source: ['./dev/fonts/**/*.*'],
			watch: './dev/fonts/**/*.*',
			destination: './pub/pages/assets/fonts/'
		},

		js: {
			source: scriptsRet(),
			watch: scriptsRet(),
			destination: './pub/js/'
		}
	};

// Локальный сервер
gulp.task('webserver', function () {
	// стучимся на /pages/
	gulp.src('pub/pages')
		.pipe(webserver({
			host: 'localhost',
			port: 420,
			fallback: 'index.html'
		}));
});



// Собираем static
gulp.task('js', function () {
	gulp
		.src(scriptsRet())
		.pipe(concat('app.min.js'))
		// минификатор
		// .pipe(uglify())
		.pipe(gulp.dest('./pub/pages/assets/js'));
});


gulp.task('stylus', function () {
	gulp
		.src(path.styl.source)
		//инициализируем soucemap
		.pipe(sourcemaps.init())
		// Компилим из Стилуса
		.pipe(stylus())
		// префиксы для ксс
		.pipe(autoprefixer({
			browsers: ['last 2 versions']
		}))
		// соединяю во едино все файлы
		.pipe(concatSourcemap(path.styl.fileName))
		// записываю сурсмап
		.pipe(sourcemaps.write())
		// минификатор
		// .pipe(csso())
		.pipe(gulp.dest(path.styl.destination));
});


// Собираем html из pug
gulp.task('pug', function () {
	gulp.src(path.pug.source)
		.pipe(pug())
		.pipe(gulp.dest(path.pug.destination))
});

// Собираем html из pug
gulp.task('svg', function () {
	return gulp.src(path.svg.source)
		.pipe(gulp.dest(path.svg.destination));
});

// Собираем html из pug
gulp.task('fonts', function () {
	return gulp.src(path.fonts.source)
		.pipe(gulp.dest(path.fonts.destination));
});


// Watch Task
gulp.task('watch', function () {
	livereload.listen();
	gulp.watch(path.js.watch, ['js']).on('change', livereload.changed);
	gulp.watch(path.svg.watch, ['svg']).on('change', livereload.changed);
	gulp.watch(path.pug.watch, ['pug']).on('change', livereload.changed);
	gulp.watch(path.fonts.watch, ['fonts']).on('fonts', livereload.changed);
	gulp.watch(path.styl.watch, ['stylus']).on('change', livereload.changed);
});

gulp.task("build", ['stylus', 'fonts', 'pug', 'svg', 'js', 'webserver']);

gulp.task("default", ['build', 'watch']);
